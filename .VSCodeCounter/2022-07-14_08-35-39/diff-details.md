# Diff Details

Date : 2022-07-14 08:35:39

Directory /home/kjoh/dmf-route-schedule

Total : 5 files,  323 codes, 58 comments, 25 blanks, all 406 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/Execution.cs](/DMFRouteSchedule/Execution.cs) | C# | -1 | 1 | 0 | 0 |
| [DMFRouteSchedule/Main.cs](/DMFRouteSchedule/Main.cs) | C# | 25 | -27 | 2 | 0 |
| [DMFRouteSchedule/OperationGraphGenerator.cs](/DMFRouteSchedule/OperationGraphGenerator.cs) | C# | 174 | 37 | 1 | 212 |
| [DMFRouteSchedule/Printer.cs](/DMFRouteSchedule/Printer.cs) | C# | 6 | 0 | 0 | 6 |
| [DMFRouteSchedule/visualize.py](/DMFRouteSchedule/visualize.py) | Python | 119 | 47 | 22 | 188 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details