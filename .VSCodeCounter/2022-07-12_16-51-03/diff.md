# Diff Summary

Date : 2022-07-12 16:51:03

Directory /home/kjoh/dmf-route-schedule

Total : 5 files,  312 codes, 128 comments, 11 blanks, all 451 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 5 | 312 | 128 | 11 | 451 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 5 | 312 | 128 | 11 | 451 |
| DMFRouteSchedule | 5 | 312 | 128 | 11 | 451 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)