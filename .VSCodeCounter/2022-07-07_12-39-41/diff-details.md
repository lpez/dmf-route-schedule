# Diff Details

Date : 2022-07-07 12:39:41

Directory /home/kjoh/dmf-route-schedule

Total : 2 files,  519 codes, 65 comments, 0 blanks, all 584 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/OperationGraph.cs](/DMFRouteSchedule/OperationGraph.cs) | C# | 85 | 9 | 0 | 94 |
| [DMFRouteSchedule/OperationGraphInitializer.cs](/DMFRouteSchedule/OperationGraphInitializer.cs) | C# | 434 | 56 | 0 | 490 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details