# Diff Details

Date : 2022-07-12 16:51:03

Directory /home/kjoh/dmf-route-schedule

Total : 5 files,  312 codes, 128 comments, 11 blanks, all 451 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/Execution.cs](/DMFRouteSchedule/Execution.cs) | C# | 0 | -1 | 0 | -1 |
| [DMFRouteSchedule/Main.cs](/DMFRouteSchedule/Main.cs) | C# | 0 | 37 | 2 | 39 |
| [DMFRouteSchedule/OperationGraph.cs](/DMFRouteSchedule/OperationGraph.cs) | C# | 15 | 6 | 2 | 23 |
| [DMFRouteSchedule/OperationGraphGenerator.cs](/DMFRouteSchedule/OperationGraphGenerator.cs) | C# | 295 | 83 | 7 | 385 |
| [DMFRouteSchedule/OperationGraphInitializer.cs](/DMFRouteSchedule/OperationGraphInitializer.cs) | C# | 2 | 3 | 0 | 5 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details