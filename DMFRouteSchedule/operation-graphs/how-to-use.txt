# The type 'a;b' is a coordinate instantiation of u.
# As it is now we require every droplet to have a position, even if this 
# droplet is the result of a merge (where we perhaps move towards 
# its merge partner rather than a coordinate). This is both easier 
# and faster to handle than implementing a possible null-value coordinate.
# '|' divides the document into three parts, 
# where the first is droplet vertices, second is operation vertices,
# third is droplet vertex ids to operation vertex ids and fourth is
# operation vertex ids to droplet vertex ids.
#
# All ids start with 0,
# all colourings start with 0 (where 0 means non-polluting)