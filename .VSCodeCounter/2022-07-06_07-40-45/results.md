# Summary

Date : 2022-07-06 07:40:45

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  2886 codes, 653 comments, 289 blanks, all 3828 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 11 | 2,691 | 586 | 231 | 3,508 |
| Python | 1 | 184 | 67 | 58 | 309 |
| XML | 1 | 11 | 0 | 0 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 13 | 2,886 | 653 | 289 | 3,828 |
| DMFRouteSchedule | 13 | 2,886 | 653 | 289 | 3,828 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)