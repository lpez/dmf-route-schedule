namespace DMFRouteSchedule
{
    public class Coordinate
    {
        public int X { get; }
        public int Y { get; }
        // In case a coordinate holds no coordinates it gets 'unassigned'.
        // For example, when we construct an operation graph from
        // an input we have to represent that certain droplet vertices
        // have uninstantiated coordinates. 
        public bool Uninstantiated { get; }

        public Coordinate(int? x, int? y)
        {
            if 
            (
                x is int x_instantiated
                && y is int y_instantiated
            )
            {
                X = x_instantiated;
                Y = y_instantiated;
            }
            else
            {
                Uninstantiated = true;
            }
        }
        
        public bool OnTopOf(Coordinate coordinate)
        {
            return (X == coordinate.X && Y == coordinate.Y);   
        }

        public void Print()
        {
            Console.WriteLine("x: {0},  y: {1}", X, Y);
        }
    }
}