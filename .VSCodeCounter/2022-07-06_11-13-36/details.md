# Details

Date : 2022-07-06 11:13:36

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  3084 codes, 690 comments, 301 blanks, all 4075 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/Coordinate.cs](/DMFRouteSchedule/Coordinate.cs) | C# | 33 | 4 | 3 | 40 |
| [DMFRouteSchedule/DMFExceptions.cs](/DMFRouteSchedule/DMFExceptions.cs) | C# | 31 | 0 | 3 | 34 |
| [DMFRouteSchedule/DMFRouteSchedule.csproj](/DMFRouteSchedule/DMFRouteSchedule.csproj) | XML | 11 | 0 | 0 | 11 |
| [DMFRouteSchedule/Execution.cs](/DMFRouteSchedule/Execution.cs) | C# | 1,012 | 311 | 65 | 1,388 |
| [DMFRouteSchedule/Main.cs](/DMFRouteSchedule/Main.cs) | C# | 19 | 17 | 10 | 46 |
| [DMFRouteSchedule/Metrics.cs](/DMFRouteSchedule/Metrics.cs) | C# | 60 | 12 | 9 | 81 |
| [DMFRouteSchedule/OperationGraph.cs](/DMFRouteSchedule/OperationGraph.cs) | C# | 170 | 22 | 17 | 209 |
| [DMFRouteSchedule/OperationGraphGenerator.cs](/DMFRouteSchedule/OperationGraphGenerator.cs) | C# | 193 | 23 | 30 | 246 |
| [DMFRouteSchedule/OperationGraphInitializer.cs](/DMFRouteSchedule/OperationGraphInitializer.cs) | C# | 792 | 96 | 53 | 941 |
| [DMFRouteSchedule/Printer.cs](/DMFRouteSchedule/Printer.cs) | C# | 250 | 26 | 24 | 300 |
| [DMFRouteSchedule/Schedule.cs](/DMFRouteSchedule/Schedule.cs) | C# | 16 | 17 | 6 | 39 |
| [DMFRouteSchedule/Topology.cs](/DMFRouteSchedule/Topology.cs) | C# | 313 | 95 | 23 | 431 |
| [DMFRouteSchedule/visualize.py](/DMFRouteSchedule/visualize.py) | Python | 184 | 67 | 58 | 309 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)