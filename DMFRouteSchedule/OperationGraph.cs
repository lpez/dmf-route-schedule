using System;
using System.Diagnostics;
namespace DMFRouteSchedule
{

    // We don't include a set of arcs AO and instead track these implicitly by letting vertices point to their successor in the operation graph
    class OperationGraph
    {
        public List<DropletVertex> VD { get; set; }
        public List<OperationVertex> VO { get; set; }
        // The first class of I should have integer class '0'
        public List<int> I_d { get; set; }
        public List<int> I_o { get; set; }
        public List<List<int>> CG { get; set; }
        public List<List<bool>> F { get; set; }
        public List<List<int>> E { get; set; }
        public List<List<bool>> K { get; set; }

        public List<DropletVertex> Roots { get; set; }
        public Dictionary<int, Operation> OperationIdToOperation { get; set; }
        public List<Operation> FirstOperations { get; set; }
        // Droplet vertex id to droplet vertex
        public Dictionary<int, DropletVertex> DIdToDv { get; set; }
        // Operation vertex id to operation vertex
        public Dictionary<int, OperationVertex> OIdToOv { get; set; }
        // Droplet vertex to operation vertex out-neighbours
        public Dictionary<int, List<int>> DIdToOIdOut { get; set;}
        // Operation vertex to droplet vertex out-neighbours
        public Dictionary<int, List<int>> OIdToDIdOut { get; set; }
        // Droplet vertex to operation vertex in-neighbours
        public Dictionary<int, List<int>> DIdToOIdIn { get; set; }
        // Operation vertex to droplet vertex in-neighbours
        public Dictionary<int, List<int>> OIdToDIdIn { get; set; }
        // Does what 'DIdToOIdOut' does, but from DropletVertex to OperationVertex
        // instead of just ids
        public Dictionary<DropletVertex, OperationVertex> DropletVertexToOperationVertex { get; set; }
        // Does what 'OIdToDIdOut' does, but from OperationVertex to DropletVertex
        // instead of just ids
        public Dictionary<OperationVertex, List<DropletVertex>> OperationVertexToDropletVertex { get; set; }
        public OperationGraph(GridConfiguration gcf, string graphInput, string mixingRules, bool feasibilityCheckInput)
        {
            var ogi = new OperationGraphInitializer(graphInput: graphInput, mixingRules: mixingRules, feasibilityCheckInput: feasibilityCheckInput);
            this.I_d = ogi.I_d;
            this.I_o = ogi.I_o;
            this.CG = ogi.GetCG(gcf);
            this.F = ogi.GetF();
            this.E = ogi.GetE();
            this.K = ogi.GetK();
            DropletVertexToOperationVertex = ogi.GetDropletVertexToOperationVertex();
            OperationVertexToDropletVertex = ogi.GetOperationVertexToDropletVertex();
            this.Roots = ogi.Roots;
            OperationIdToOperation = ogi.OIdToOperations;
            FirstOperations = ogi.GetFirstOperations();
            DIdToDv = ogi.DIdToDv;
            OIdToOv = ogi.OIdToOv;
            DIdToOIdOut = ogi.DIdToOIdOut;
            OIdToDIdOut = ogi.OIdToDIdOut;
            DIdToOIdIn = ogi.DIdToOIdIn;
            OIdToDIdIn = ogi.OIdToDIdIn;
        }
    }

    class DropletVertex
    {
        public int DropletId { get; set; }
        public Coordinate? Coordinate { get; set; }
        // If this DropletVertex has volume V, 
        // then this.Volume = V/pi.
        // This transformation means less computation
        // when using neighbourhood methods.
        public double Volume { get; set; }
        public int Colour { get; set; }

        // The general constructor with feasibility checks
        public DropletVertex(string d, string u, string x, string c)
        {
            // Try to assign 'DropletId'
            try { DropletId = Int32.Parse(d); } catch (System.FormatException e)
            {
                var errorMessage = String.Format
                (
                    "Droplet id not feasible: '{0}'",
                    d
                );
                throw new InputNotFeasibleException(errorMessage);
            }
            // Try to assign 'Coordinate'
            if (u == "_")
            {
                Coordinate = new Coordinate(null, null);
            }
            else
            {
                string[] split = u.Split(";");
                if (split.Count() == 2)
                {
                    try
                    {
                        Coordinate = new Coordinate
                        (
                            Int32.Parse(split[0]),
                            Int32.Parse(split[1])
                        );
                    }
                    catch(System.FormatException e)
                    {
                        var errorMessage = String.Format
                        (
                            "Coordinate is not feasible: '{0}'",
                            u
                        );
                        throw new InputNotFeasibleException(errorMessage);
                    }
                }
                else
                {
                    var errorMessage = String.Format
                    (
                        "Coordinate is not feasible: '{0}'",
                        u
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
            }
            // Try to assign 'Volume'
            try { Volume = Double.Parse(x); } catch (System.FormatException e)
            {
                var errorMessage = String.Format
                (
                    "Volume is not feasible: '{0}'",
                    x
                );
                throw new InputNotFeasibleException(errorMessage);
            }
            // Try to assign 'Colour'
            try { Colour = Int32.Parse(c); } catch (System.FormatException e)
            {
                var errorMessage = String.Format
                (
                    "Colour class not feasible: '{0}'",
                    c
                );
                throw new InputNotFeasibleException(errorMessage);
            }
        }

        // A specialized constructor. IMPORTANT!!!! Use only when certain that
        // the input parameters will give a feasible droplet vertex
        public DropletVertex(int dropletId, Coordinate coordinate, double volume, int colour)
        {
            DropletId = dropletId;
            Coordinate = coordinate;
            Volume = volume;
            Colour = colour;
        }

    }

    class OperationVertex
    {
        // m now holds an integer type instead of a move name for comparison reasons
        public int O { get; set; }
        public int M { get; set; }
        public int BigT { get; set; }
        public int LittleT { get; set; }
        public double P { get; set; }

        // The general constructor with feasibility checks
        public OperationVertex(string oId, string moveName, string T, string t, string p)
        {
            // Try to assign 'O'
            try { O = Int32.Parse(oId); } catch (System.FormatException e)
            {
                var errorMessage = String.Format
                (
                    "Operation id not feasible: {0}",
                    oId
                );
                throw new InputNotFeasibleException(errorMessage); 
            }
            // Format checking handled by 'MoveInformation.MoveNameToType(...)'            
            M = MoveInformation.MoveNameToType(moveName);
            // Try to assign 'T'
            if (T != "_")
            {
                try { BigT = Int32.Parse(T); } catch(System.FormatException e)
                {
                    var errorMessage = String.Format
                    (
                        "T is not feasible: {0}",
                        T
                    );
                    throw new InputNotFeasibleException(errorMessage); 
                }
            } else { BigT = Int32.MinValue; }
            // Try to assign 't'
            if (t != "_")
            {
                try { LittleT = Int32.Parse(t); } catch(System.FormatException e)
                {
                    var errorMessage = String.Format
                    (
                        "t is not feasible: {0}",
                        t
                    );
                    throw new InputNotFeasibleException(errorMessage); 
                }
            } else { LittleT = Int32.MaxValue; }
            // Try to assign 'P'
            try {P = Double.Parse(p); } catch(System.FormatException e)
            {
                var errorMessage = String.Format
                (
                    "P is not feasible: {0}",
                    P
                );
                throw new InputNotFeasibleException(errorMessage); 
            }
        }

        // A specialized constructor. IMPORTANT!!!! Use only when certain that
        // the input parameters will give a feasible droplet vertex
        public OperationVertex(int oId, int moveNumber, int T, int t, double p)
        {
            O = oId;
            M = moveNumber;
            BigT = T;
            LittleT = t;
            P = p;
        }
    }

    // Represents an operation
    class Operation
    {
        public List<DropletVertex> DvsIn {get; set;}
        public OperationVertex Ov {get; set;}
        public List<DropletVertex> DvsOut {get; set;}

        public Operation()
        {
            this.DvsIn = new List<DropletVertex>();
            this.DvsOut = new List<DropletVertex>();
        }

        public Operation(List<DropletVertex> dvsIn, OperationVertex ov, List<DropletVertex> dvsOut)
        {
            this.DvsIn = dvsIn;
            this.Ov = ov;
            this.DvsOut = dvsOut;
        }

        public void AddInNeighbour(DropletVertex dvIn)
        {
            this.DvsIn.Add(dvIn);
        }

        public void AddOutNeighbour(DropletVertex dvOut)
        {
            this.DvsOut.Add(dvOut);
        }
    }

    // Contains information about the operation names and allows for translation from operation names to unique vertices.
    // The move names and type integers correspond to the order given in chapter 5, "Operation vertex".
    static class MoveInformation
    {
        static List<string> moveNames = new List<string>(6)
        {
            "Move",
            "Merge",
            "Mix",
            "Split",
            "Dispense",
            "Heat"
        };

        public static List<string> MoveNames()
        {
            return moveNames;
        }

        // Translates a move name to the according move type integer
        public static int MoveNameToType(string moveName)
        {
            if (!moveNames.Contains(moveName))
            {
                var errorMessage = String.Format
                (
                    "Operation not feasible: {0}",
                    moveName
                );
                throw new InputNotFeasibleException(errorMessage); 
            }
            return moveName switch
                {
                    "Move" => 1,
                    "Merge" => 2,
                    "Mix" => 3,
                    "Split" => 4,
                    "Dispense" => 5,
                    "Heat" => 6
                };
        }

        // Translates a move type integer to the according move name
        public static string TypeToMoveName(int moveType)
        {
            Debug.Assert
            (
                ( moveType >= 1 ) && ( moveType <= 6 ), 
                "The given move type is not valid." 
            );
            return moveType switch
                {
                    1 => "Move",
                    2 => "Merge",
                    3 => "Mix",
                    4 => "Split",
                    5 => "Dispense",
                    6 => "Heat"
                };
        }

    }
}