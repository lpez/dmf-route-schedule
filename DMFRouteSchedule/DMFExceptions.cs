using System;

namespace DMFRouteSchedule
{
    [Serializable]
    public class NoAvailableMoveException : Exception
    {
        public NoAvailableMoveException() : base() {}
        public NoAvailableMoveException(string message) : base(message) {}
        public NoAvailableMoveException(string message, Exception inner) : base(message, inner) {}
        protected NoAvailableMoveException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class MaximumStepsReachedException : Exception
    {
        public MaximumStepsReachedException() : base() {}
        public MaximumStepsReachedException(string message) : base(message) {}
        public MaximumStepsReachedException(string message, Exception inner) : base(message, inner) {}
        protected MaximumStepsReachedException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }

    [Serializable]
    public class InputNotFeasibleException : Exception
    {
        public InputNotFeasibleException() : base() {}
        public InputNotFeasibleException(string message) : base(message) {}
        public InputNotFeasibleException(string message, Exception inner) : base(message, inner) {}
        protected InputNotFeasibleException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
    }
}