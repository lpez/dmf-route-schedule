using System;

namespace DMFRouteSchedule
{
    // Metrics
    public static class Metrics
    {
        // Returns LP1-distance
        public static int LP1(Coordinate from, Coordinate to)
        {
            return 
            Math.Max(from.X - to.X, to.X - from.X) 
            + Math.Max(from.Y - to.Y, to.Y - from.Y);
        }

        // Returns horizontal distance
        public static int HorizontalDistance(Coordinate from, Coordinate to)
        {
            return Math.Max(from.X - to.X, to.X - from.X);
        }

        // Returns vertical distance
        public static int VerticalDistance(Coordinate from, Coordinate to)
        {
            return Math.Max(from.Y - to.Y, to.Y - from.Y);
        }

        public static int ComputeCriticalDistance(double dv1Volume, double dv2Volume)
        {
            // Amount of cells from center to border in horizontal or vertical direction
            int dv1Radius = 
            dv1Volume < 2.3? 1 :
            dv1Volume >= 2.3 && dv1Volume < 18? 2 :
            3;
            // Same as above
            int dv2Radius =
            dv2Volume < 2.3? 1 :
            dv2Volume >= 2.3 && dv2Volume < 18? 2 :
            3;

            return dv1Radius + dv2Radius;
        }
    }

    // Metrics, but we precompute -- to test difference in speed
    // *TODO WHEN NEEDED* 
    class MetricsPrecompute
    {
        private Dictionary<Coordinate, Dictionary<Coordinate, int>> lp1;
        
        public MetricsPrecompute(int width, int height, params string[] norms)
        {
            foreach (string norm in norms){
                if (norm == "LP1")
                {
                    InitLP1(width, height);
                }
                else if (norm == "LP2")
                {
                    // TODO
                }
                // And so on
                else
                {
                    throw new ArgumentException("Invalid norm", norm);
                }
            }
        }

        public int LP1() // parameters should denote a coordinate
        {
            // return 'lp1'
            return 0;
        }

        private void InitLP1(int width, int height)
        {
            // TODO
        }
    }
}