using System;

namespace DMFRouteSchedule
{
    public class MainClass
    {
        public static void Main(String[] args)
        {
            
            var VS = new List<(Coordinate, int type)>() { (new Coordinate(1, 1), 10) };
            var gcf = new GridConfiguration(20, 32, VS);

            /*

            string graphInput = "generated-operation-graphs/graph-generator1-generated-graph79.txt";
            string rulesInput = "generated-rulesets/graph-generator1-generated-rules79.txt";
            string executionOutput = "execution-output/";

            // TESTING GRAPH AND RULE GENERATOR
            string graphInput = "operation-graph-generators/graph-generator1.txt";
            string graphOutput = "generated-operation-graphs/generated-graph1.txt"; 
            // Probability distribution of picking a new operation
            double[] operationDistribution = new double[6] { 0.2, 0.2, 0.2, 0.2, 0.2, 0 };
            // DropletInfo
            var dropletInfo = new DropletInfo
            (
                volumeIntervalLowerBound: 2, volumeIntervalUpperBound: 80
            );
            // OperationInfo
            var operationInfo = new OperationInfo
            (
                bigTIntervalLowerBound: 0, bigTIntervalUpperBound: 30,
                littleTIntervalLowerBound: 30, littleTIntervalUpperBound: 100,
                pIntervalLowerBound: 0.1, pIntervalUpperBound: 1
            );
            // Generator
            var ogGenerator = new OperationGraphGenerator(20);
            // GENERATE GRAPH
            ogGenerator.GenerateGraph
            (
                inputFileName: graphInput, outputFileName: graphOutput, 
                steps: 20, pNew: 0.5, pOperation: operationDistribution,
                dInfo: dropletInfo, oInfo: operationInfo
            );
            // GENERATE RULES
            string ruleOutput = "generated-rulesets/generated-ruleset1.txt";
            ogGenerator.GenerateRules
            (
                inputFileName: graphOutput, outputFileName: ruleOutput
            );
            */

            // TESTING MERGE
            // string graphInput = "operation-graphs/graph1.txt";
            // string mixingRules = "mixing-rules/ruleset1.txt";

            // TESTING MERGE + MOVE
            // string graphInput = "operation-graphs/graph2.txt";
            // string mixingRules = "mixing-rules/ruleset2.txt";

            // TESTING MERGE AND MOVEMENT AROUND THE BORDER
            // string graphInput = "operation-graphs/graph-merge-and-move-at-border.txt";
            // string mixingRules = "mixing-rules/ruleset2.txt";

            // TESTING SPLIT
            // string graphInput = "operation-graphs/graph-split.txt";
            // string mixingRules = "mixing-rules/ruleset1.txt";

            // TESTING DISPENSE
            // string graphInput = "operation-graphs/graph-dispense.txt";
            // string mixingRules = "mixing-rules/ruleset1.txt";

            // TESTING MIX
            // string graphInput = "operation-graphs/graph-mix.txt";
            // string mixingRules = "mixing-rules/ruleset2.txt";
            
            
            // TESTING GENERATED GRAPH AND RULES
            // graphInput = "generated-operation-graphs/generated-graph1.txt";
            // string mixingRules = "generated-rulesets/generated-ruleset1.txt";

            /*
            // GENERATE SEVERAL OPERATION GRAPHS
            string graphInputDirectory = "operation-graph-generators/";
            string graphInputName = "graph-generator1.txt";
            string graphOutputDirectory = "generated-operation-graphs/";
            string rulesOutputDirectory = "generated-rulesets/";
            // Probability distribution of picking a new operation
            double[] operationDistribution = new double[6] { 0.2, 0.2, 0.2, 0.2, 0.2, 0 };
            // DropletInfo
            var dropletInfo = new DropletInfo
            (
                volumeIntervalLowerBound: 2, volumeIntervalUpperBound: 80
            );
            // OperationInfo
            var operationInfo = new OperationInfo
            (
                bigTIntervalLowerBound: 0, bigTIntervalUpperBound: 30,
                littleTIntervalLowerBound: 30, littleTIntervalUpperBound: 100,
                pIntervalLowerBound: 0.1, pIntervalUpperBound: 1
            );

            OperationGraphGenerator ogg = new OperationGraphGenerator(20);
            ogg.GenerateSeveral
            (
                graphInputDirectory: graphInputDirectory, graphInputName: graphInputName,
                graphOutputDirectory: graphOutputDirectory, rulesOutputDirectory: rulesOutputDirectory,
                numberOfSteps: 20, numberOfOperationGraphs: 100, pNew: 0.5,
                pOperation: operationDistribution, dInfo: dropletInfo, oInfo: operationInfo
            );
            */
            
// ---------------------------------------------------------------------------
// TESTING HEURISTIC1 ON ALL GENERATED GRAPHS
            /*
            Reset();
            string graphDirectory = "generated-operation-graphs/";
            string rulesDirectory = "generated-rulesets/";
            int graphFirstIndex = graphDirectory.Count();
            int rulesFirstIndex = rulesDirectory.Count();
            string[] generatedGraphsNames = 
                    Directory.GetFiles(graphDirectory)
                    .OrderBy(graph => GetGraphOrderNumber(graph))
                    .ToArray();
            string[] generatedRulesNames = 
                    Directory.GetFiles(rulesDirectory)
                    .OrderBy(graph => GetRulesOrderNumber(graph))
                    .ToArray();
            // Assuming 'generatedGraphs' and 'generatedRules' include graphs and rules in order
            // of correspondance
            for (int i = 2; i < 3; i++)
            //for (int i = 0; i < generatedGraphsNames.Count(); i++)
            {
                // Paths to the graph and to the rules
                string graphPath = generatedGraphsNames[i];
                string rulesPath = generatedRulesNames[i];
                // Construction of output folder
                int graphNameLength = graphPath.Count() - graphFirstIndex - 4;
                string graphName = graphPath.Substring(graphFirstIndex, graphNameLength);
                string outputDirectory = "execution-output/" + graphName + "/";
                // Solvping the input
                var og = new OperationGraph(gcf, graphPath, rulesPath, false);
                var executor = new Executor(og, gcf, seed: 20);
                try
                {
                    executor.Heuristic1
                    (
                        numberOfIterations: 500, 
                        shouldPrintText: false, 
                        shouldPrintPictures: true, 
                        outputDirectory: outputDirectory
                    );
                } catch (DMFRouteSchedule.NoAvailableMoveException e)
                {
                    Console.WriteLine("Error finishing graph {0}", i);
                    continue;
                }
                
            } 
            */
// ---------------------------------------------------------------------------
// ATTEMPT AT PREDICTING TIME CONSUMED BY NEW HEURISTIC
// 1000 large modifications with heavy overload takes about 3 minutes to compute,
// hence 10000 such large modifications with heavy overload would take about 30 minutes.
// This should give incentive to try and develop a heavier heuristc.

            string graphInput = "operation-graphs/graph-mix.txt";
            string mixingRules = "mixing-rules/ruleset1.txt";
            OperationGraph og = new OperationGraph(gcf: gcf, graphInput: graphInput, 
                                                    mixingRules: mixingRules, 
                                                    feasibilityCheckInput: false);
            ExecutorTest execTest = new ExecutorTest(og: og, gcf: gcf, seed: 10);
        
            execTest.TestCGCollectionTime(amountOfModifications: 10000, extraWorkPerModification: 41000000);
            //execTest.TestCGCollectionTime(amountOfModificationsPerStep: 20, amountOfSteps: 100, extraWorkPerModification: 41000000);
        }

        private static void Reset()
        {
            Directory.Delete("execution-output", true);
            Directory.CreateDirectory("execution-output");
        }

        // Given a generated graph input name, returns its number
        private static int GetGraphOrderNumber(string graphInputString)
        {
            int i = graphInputString.Count() - 5;
            string number = "";
            while ( graphInputString[i] != 'h')
            {
                number = graphInputString[i] + number;
                i--;
            }
            return Int32.Parse(number);
        }

        // Given a generated ruleset input name, returns its number
        private static int GetRulesOrderNumber(string rulesInputString)
        {
            int i = rulesInputString.Count() - 5;
            string number = "";
            while ( rulesInputString[i] != 's')
            {
                number = rulesInputString[i] + number;
                i--;
            }
            return Int32.Parse(number);
        }

    }
}