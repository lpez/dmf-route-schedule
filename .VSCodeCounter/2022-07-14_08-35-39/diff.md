# Diff Summary

Date : 2022-07-14 08:35:39

Directory /home/kjoh/dmf-route-schedule

Total : 5 files,  323 codes, 58 comments, 25 blanks, all 406 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 4 | 204 | 11 | 3 | 218 |
| Python | 1 | 119 | 47 | 22 | 188 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 5 | 323 | 58 | 25 | 406 |
| DMFRouteSchedule | 5 | 323 | 58 | 25 | 406 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)