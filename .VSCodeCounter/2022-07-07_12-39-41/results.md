# Summary

Date : 2022-07-07 12:39:41

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  3603 codes, 755 comments, 301 blanks, all 4659 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 11 | 3,408 | 688 | 243 | 4,339 |
| Python | 1 | 184 | 67 | 58 | 309 |
| XML | 1 | 11 | 0 | 0 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 13 | 3,603 | 755 | 301 | 4,659 |
| DMFRouteSchedule | 13 | 3,603 | 755 | 301 | 4,659 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)