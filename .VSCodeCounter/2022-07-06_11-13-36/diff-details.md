# Diff Details

Date : 2022-07-06 11:13:36

Directory /home/kjoh/dmf-route-schedule

Total : 5 files,  198 codes, 37 comments, 12 blanks, all 247 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/DMFExceptions.cs](/DMFRouteSchedule/DMFExceptions.cs) | C# | 9 | 0 | 1 | 10 |
| [DMFRouteSchedule/Execution.cs](/DMFRouteSchedule/Execution.cs) | C# | -1 | 0 | 0 | -1 |
| [DMFRouteSchedule/Main.cs](/DMFRouteSchedule/Main.cs) | C# | 0 | 0 | -1 | -1 |
| [DMFRouteSchedule/OperationGraph.cs](/DMFRouteSchedule/OperationGraph.cs) | C# | 0 | 0 | -1 | -1 |
| [DMFRouteSchedule/OperationGraphInitializer.cs](/DMFRouteSchedule/OperationGraphInitializer.cs) | C# | 190 | 37 | 13 | 240 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details