# Diff Summary

Date : 2022-07-07 12:39:41

Directory /home/kjoh/dmf-route-schedule

Total : 2 files,  519 codes, 65 comments, 0 blanks, all 584 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 2 | 519 | 65 | 0 | 584 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 2 | 519 | 65 | 0 | 584 |
| DMFRouteSchedule | 2 | 519 | 65 | 0 | 584 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)