using System;
using System.IO;
using System.Diagnostics;

// TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//
//
//
//
//
//
//     
//
//
//
//
//
//
//
//
//

namespace DMFRouteSchedule
{
    // When we want to take a file file path and initialize an operation graph
    class OperationGraphInitializer
    {   
        // Used to flag whether initialization output should be printed to the console or not
        private bool shouldPrint = false;
        private string graphInput;
        private string mixingRules;
        // The ids of all droplet vertices
        public List<int> I_d { get; set; }
        // The ids of all operation vertices
        public List<int> I_o { get; set; }
        // Droplet vertex id to droplet vertex
        public Dictionary<int, DropletVertex> DIdToDv { get; set; }
        // Operation vertex id to operation vertex
        public Dictionary<int, OperationVertex> OIdToOv { get; set; }
        // Droplet vertex to operation vertex out-neighbours
        public Dictionary<int, List<int>> DIdToOIdOut { get; set;}
        // Operation vertex to droplet vertex out-neighbours
        public Dictionary<int, List<int>> OIdToDIdOut { get; set; }
        // Droplet vertex to operation vertex in-neighbours
        public Dictionary<int, List<int>> DIdToOIdIn { get; set; }
        // Operation vertex to droplet vertex in-neighbours
        public Dictionary<int, List<int>> OIdToDIdIn { get; set; }
        // A dictionary mapping operation ids to triples representing operation vertices
        public Dictionary<int, Operation> OIdToOperations { get; set; }

        public List<DropletVertex> Roots { get; set; }

        // Constructor when we want to generate from input
        public OperationGraphInitializer(string graphInput, string mixingRules, bool feasibilityCheckInput)
        {
            this.graphInput = File.ReadAllText(graphInput);
            this.mixingRules = File.ReadAllText(mixingRules);


            DIdToDv = new Dictionary<int, DropletVertex>();
            OIdToOv = new Dictionary<int, OperationVertex>();
            DIdToOIdOut = new Dictionary<int, List<int>>();
            OIdToDIdOut = new Dictionary<int, List<int>>();
            DIdToOIdIn = new Dictionary<int, List<int>>();
            OIdToDIdIn = new Dictionary<int, List<int>>();
            OIdToOperations = new Dictionary<int, Operation>();
            InitDictionaries();
            InitI_d();
            InitI_o();
            InitOperations();
            InitRoots();
            if (feasibilityCheckInput)
            {
                if (InputNotFeasible(out string errorMessage))
                {
                    throw new InputNotFeasibleException(errorMessage);
                }
            }
        }

        // Constructor when we want to auto-generate an input
        public OperationGraphInitializer()
        {
            DIdToDv = new Dictionary<int, DropletVertex>();
            OIdToOv = new Dictionary<int, OperationVertex>();
            DIdToOIdOut = new Dictionary<int, List<int>>();
            OIdToDIdOut = new Dictionary<int, List<int>>();
            DIdToOIdIn = new Dictionary<int, List<int>>();
            OIdToDIdIn = new Dictionary<int, List<int>>();
        }

        // Returns the colouring function CG
        public List<List<int>> GetCG(GridConfiguration gcf)
        {
            var CG = new List<List<int>>(gcf.Width);
            for (int i = 0; i < gcf.Width; i ++)
            {
                CG.Add
                (
                    Enumerable.Range(0, gcf.Height).Select(x => 0).ToList()
                );
            }
            foreach (DropletVertex dv in this.Roots)
            {
                foreach ( Coordinate U in gcf.NX(dv.Coordinate, dv.Volume ) )
                {
                    CG[U.X][U.Y] = dv.Colour;
                }
            }
            return CG;
        }

        // Returns the tabula deciding whether two droplet colours can cross paths
        public List<List<bool>> GetF()
        {
            int[] allCcs = 
            mixingRules.Split("|\n")[0]
            .Split("\n", StringSplitOptions.RemoveEmptyEntries)
            .Select( x => Int32.Parse(x) )
            .ToArray();

            var F = new List<List<bool>>(allCcs.Count());


            // Initialize every crossing rule to default false
            for (int i = 0; i < allCcs.Count(); i++)
            {
                F.Add
                (
                    Enumerable.Range(0, allCcs.Count()).Select(x => false).ToList()
                );
            }

            // Adds rules for self-crossing and crossing non-polluted area
            for (int i = 0; i < allCcs.Count(); i++)
            {
                F[i][i] = true;
                F[i][0] = true;
            }

            // Adds rules for merges
            foreach (Operation o in this.OIdToOperations.Values)
            {
                if (o.Ov.M == 2)
                {
                    // Now we have a merge by definition of 'MoveInformation' class
                    int in1 = o.DvsIn[0].Colour;
                    int in2 = o.DvsIn[1].Colour;
                    int ud = o.DvsOut[0].Colour;
                    
                    F[in1][in2] = true;
                    F[in2][in1] = true;
                    F[ud][in1] = true;
                    F[ud][in2] = true;
                }
            }
            if (shouldPrint)
            {
                PrintF(F);
            }
            return F;
        }
        // Used for printing 'F' when initializing
        private void PrintF( List<List<bool>> F)
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("F:");
            for (int i = 0; i < F.Count; i++)
            {
                for (int j = 0; j < F.Count; j++)
                {
                    Console.WriteLine
                    (
                        "i:  {0},   j:  {1},   can cross:  {2}",
                        i,
                        j,
                        F[i][j]
                    );
                }
            }
            Console.WriteLine("\n \n");
        }


        // returns the tabula deciding what droplet class results from mixing two other droplets
        public List<List<int>> GetE()
        {
            string[] allCcs = this.mixingRules.Split("|\n")[0].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            string[] ccRules = this.mixingRules.Split("|\n")[1].Split("\n", StringSplitOptions.RemoveEmptyEntries);

            // Sets E[c1][c2] = 0 for all c1 and c2 not equal to 0,
            // sets E[c1][c1] = c1
            // Sets E[0][c1] = c1
            var E = new List<List<int>>(allCcs.Count());
            for (int i = 0; i < allCcs.Count(); i++)
            {
                E.Add
                (
                    Enumerable.Range(0, allCcs.Count()).Select(x => 0).ToList()
                );
                E[i][i] = i;
                E[0][i] = i;
                E[i][0] = i;
            }
            foreach (string ccRule in ccRules)
            {
                string[] ccs = ccRule.Split(", ");
                int cc1 = Int32.Parse(ccs[0]);
                int cc2 = Int32.Parse(ccs[1]);
                int ccResult = Int32.Parse(ccs[2]);

                E[cc1][cc2] = ccResult;
                E[cc2][cc1] = ccResult;
            }
            if (shouldPrint)
            {
                PrintE( E );
            }
            
            return E;
        }

        // Used for printing E when initializing
        private void PrintE( List<List<int>> E)
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("E:");
            for (int i = 0; i < E.Count; i++)
            {
                for (int j = 0; j < E.Count; j++)
                {
                    Console.WriteLine
                    (
                        "i:  {0},   j:  {1},   resulting colour:  {2}",
                        i,
                        j,
                        E[i][j]
                    );
                }
            }
            Console.WriteLine("\n \n");
        }
        // IMPORTANT!!! CAN BE REMOVED:
        //
        // Tests whether the given 'E' is feasible by checking that
        // E(a, E(b, c)) = E(E(a, b), c) holds for all colours a, b, c
        private bool EIsFeasible( List<List<int>> E)
        {
            for (int i = 0; i < E.Count; i++)
            {
                for (int j = 0; j < E.Count; j++)
                {
                    for (int l = 0; l < E.Count; l++)
                    {
                        if ( E[i][E[j][l]] != E[E[i][j]][l] )
                        {
                            Console.WriteLine("i:  {0},    j:  {1},    l:  {2}", i, j, l);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        // Returns the tabula deciding whether a droplet can enter another droplet's interference region.
        // Here the keys are droplet id's
        public List<List<bool>> GetK()
        {
            var K = new List<List<bool>>(this.I_d.Count);
            for (int i = 0; i < this.I_d.Count; i++)
            {
                K.Add
                (
                    Enumerable.Range(0, this.I_d.Count).Select(x => false).ToList()
                );
            }
            // A droplet can always pass its own interference zone
            for (int i = 0; i < this.I_d.Count; i++)
            {
                K[i][i] = true;
            }

            var mergeOperations = 
            (
                from operation in OIdToOperations.Values
                where operation.Ov.M == 2
                select operation
            );
            foreach (Operation mo in mergeOperations)
            {
                int dId1 = mo.DvsIn[0].DropletId;
                int dId2 = mo.DvsIn[1].DropletId;
                K[dId1][dId2] = true;
                K[dId2][dId1] = true;
            }
            if (shouldPrint)
            {
                //PrintK( K );
            }
            return K;   
        }

        // Used for printing K when initializing.
        private void PrintK( List<List<bool>> K)
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("K:");
            for (int i = 0; i < K.Count; i++)
            {
                for (int j = 0; j < K.Count; j++)
                {
                    Console.WriteLine
                    (
                        "i:  {0},   j:  {1},   can cross:  {2}",
                        i,
                        j,
                        K[i][j]
                    );
                }
            }
            Console.WriteLine("\n \n");
        }

        // Initializes 'DropletVertexToOperationVertex' dictionary
        public Dictionary<DropletVertex, OperationVertex> GetDropletVertexToOperationVertex()
        {
            var dict = new Dictionary<DropletVertex, OperationVertex>();
            foreach (int dId in DIdToOIdOut.Keys)
            {
                dict.Add
                (
                    DIdToDv[dId],
                    OIdToOv
                    [
                        DIdToOIdOut[ dId ][0]
                    ]
                );
            }
            return dict;
        }

        // Initializes 'OperationVertexToDropletVertex' dictionary
        public Dictionary<OperationVertex, List<DropletVertex>> GetOperationVertexToDropletVertex()
        {
            var dict = new Dictionary<OperationVertex, List<DropletVertex>>();
            foreach (int oId in OIdToDIdOut.Keys)
            {
                dict.Add
                (
                    OIdToOv[oId],
                    OIdToDIdOut[oId].Select( dId => DIdToDv[dId] ).ToList()
                );
            }
            return dict;
        }

        // Initializes the set of droplet vertex roots
        public void InitRoots()
        {
            this.Roots =  
            (
                from dId in this.DIdToDv.Keys
                where !DIdToOIdIn.Keys.Contains(dId)
                select this.DIdToDv[dId]
            ).ToList();
            if ( shouldPrint )
            {
                PrintRoots();
            }
        }

        // Prints the list of roots when they have been initialized
        // by 'InitRoots()'
        private void PrintRoots()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("Roots:");
            foreach (DropletVertex dv in Roots)
            {
                Console.WriteLine
                (
                    "Dv:  {0}, ({1}, {2}), {3}, {4}", 
                    dv.DropletId,
                    dv.Coordinate.X,
                    dv.Coordinate.Y,
                    dv.Volume,
                    dv.Colour
                );
            }
            Console.WriteLine("\n \n");
        }


        // Initializes the set droplet ids
        private void InitI_d()
        {
            I_d = DIdToDv.Keys.ToList();
        }

        // Initializes the set of operation ids
        private void InitI_o()
        {
            I_o = OIdToOv.Keys.ToList();
        }

        // Initializes the dictionaries and checks that the given input
        // has correct format
        private void InitDictionaries()
        {
            string[] split = this.graphInput.Split("|");
            if (split.Count() != 4)
            {
                throw new InputNotFeasibleException
                ("Graph input does not contain exactly 4 lines '|'");
            }
            // Initialize DIdToDv
            string[] dVertices = split[0].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (string dVertex in dVertices)
            {
                // Try to parse and add
                int dId;
                try { dId = Int32.Parse( dVertex.Split(", ")[0] ); } catch (System.FormatException e)
                {
                    var errorMessage = String.Format
                    (
                        "Droplet id not feasible: '{0}'",
                        dVertex.Split(", ")[0]
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                try
                {
                    this.DIdToDv.Add(dId, InitDropletVertex(dVertex));
                } 
                catch (System.ArgumentException e)
                {
                    var errorMessage = String.Format
                    (
                        "There exist two droplet vertices with droplet id {0}",
                        dId
                    );
                    throw new InputNotFeasibleException(errorMessage);
                } 
            }
            // Initialize oIdToOv
            string[] oVertices = split[1].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (string oVertice in oVertices)
            {
                // Try to parse and add
                int oId;
                try { oId = Int32.Parse( oVertice.Split(", ")[0] ); } catch (System.FormatException e)
                {
                    var errorMessage = String.Format
                    (
                        "Operation id not feasible: '{0}'",
                        oVertice.Split(", ")[0]
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                try
                {
                    this.OIdToOv.Add(oId, InitOperationVertex(oVertice));
                }
                catch (System.ArgumentException e) 
                {
                    var errorMessage = String.Format
                    (
                        "There exist two operation vertices with operation id {0}",
                        oId
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
            }
            // Initialize DIdToOIdOut and OIdToDIdIn
            foreach (int dId in this.DIdToDv.Keys)
            {
                this.DIdToOIdOut = new Dictionary<int, List<int>>(DIdToDv.Keys.Count);
                this.OIdToDIdIn = new Dictionary<int, List<int>>(DIdToDv.Keys.Count);
            }
            string[] arcRelations = split[2].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (string arcRelation in arcRelations)
            {
                string[] inAndOut = arcRelation.Split(" -> ");
                // We make sure that the format is correct
                if (inAndOut.Count() != 2)
                {
                    var errorMessage = String.Format
                    (
                        "Relationship does not have correct format: {0}",
                        arcRelation
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                int oId;
                List<int> dIds;
                // Try to parse and add
                try
                {
                    oId = Int32.Parse(inAndOut[1]);
                    // We make sure that the relationship only contains operation ids
                    // of operations that exist
                    if (!OIdToOv.ContainsKey(oId))
                    {
                        var errorMessage = String.Format
                        (
                            "Relationship {0} contains a non-existent operation id: {1}",
                            arcRelation,
                            oId
                        );
                        throw new InputNotFeasibleException(errorMessage);
                    }
                    dIds = 
                    (
                        from dId in inAndOut[0].Split("; ")
                        where !dId.Equals("_")
                        select Int32.Parse(dId)
                    ).ToList();
                } catch(System.FormatException e)
                // We make sure to catch any case where the relation doesn't contain either integers or underscore
                {
                    var errorMessage = String.Format
                    (
                        "Relationship can only contain integers or '_' (the underscore symbol): {0}",
                        arcRelation
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                // We make sure to catch any case where an operation vertex
                // has more than two in-neighbour dorplet vertices
                if (dIds.Count() > 2)
                {
                    var errorMessage = String.Format
                    (
                        "Relationship has more than two in-neighbour droplet ids: {0}",
                        arcRelation
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                foreach (int dId in dIds)
                {
                    // We make sure that the relationship only contains droplet ids
                    // of droplets that exist
                    if (!DIdToDv.ContainsKey(dId))
                    {
                        var errorMessage = String.Format
                        (
                            "Relationship {0} contains a non-existent droplet id: {1}",
                            arcRelation,
                            dId
                        );
                        throw new InputNotFeasibleException(errorMessage);
                    }
                    this.DIdToOIdOut.Add(dId, new List<int>(1){ oId });
                }
                this.OIdToDIdIn.Add(oId, dIds);
            }

            // Initialize OIdToDIdOut and DIdToOIdIn 
            foreach (int oId in this.OIdToOv.Keys)
            {
                this.OIdToDIdOut = new Dictionary<int, List<int>>(OIdToOv.Keys.Count);
                this.DIdToOIdIn = new Dictionary<int, List<int>>(OIdToOv.Keys.Count);
            }
            arcRelations = split[3].Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (string arcRelation in arcRelations)
            {
                string[] inAndOut = arcRelation.Split(" -> ");
                // We make sure that the format is correct
                if (inAndOut.Count() != 2)
                {
                    var errorMessage = String.Format
                    (
                        "Relationship does not have correct format: {0}",
                        arcRelation
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                int oId;
                List<int> dIds;
                // Try to parse and add
                try
                {
                    oId = Int32.Parse(inAndOut[0]);
                    // We make sure that the relationship only contains operation ids
                    // of operations that exist
                    if (!OIdToOv.ContainsKey(oId))
                    {
                        var errorMessage = String.Format
                        (
                            "Relationship {0} contains a non-existent operation id: {1}",
                            arcRelation,
                            oId
                        );
                        throw new InputNotFeasibleException(errorMessage);
                    }
                    dIds =
                    (
                        from dId in inAndOut[1].Split("; ")
                        where !dId.Equals("_")
                        select Int32.Parse(dId)
                    ).ToList();
                } catch(System.FormatException)
                // We make sure to catch any case where the relation doesn't contain either integers or underscore
                {
                    var errorMessage = String.Format
                    (
                        "Infeasible relationship: {0}",
                        arcRelation
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                // We make sure to catch any case where an operation vertex
                // has more than two out-neighbour dorplet vertices
                if (dIds.Count() > 2)
                {
                    var errorMessage = String.Format
                    (
                        "Relationship has more than two out-neighbour droplet ids: {0}",
                        arcRelation
                    );
                    throw new InputNotFeasibleException(errorMessage);
                }
                foreach (int dId in dIds)
                {
                    // We make sure that the relationship only contains droplet ids
                    // of droplets that exist
                    if (!DIdToDv.ContainsKey(dId))
                    {
                        var errorMessage = String.Format
                        (
                            "Relationship {0} contains a non-existent droplet id: {1}",
                            arcRelation,
                            dId
                        );
                        throw new InputNotFeasibleException(errorMessage);
                    }
                    this.DIdToOIdIn.Add(dId, new List<int>(1) { oId });
                }
                this.OIdToDIdOut.Add(oId, dIds);
            }
            if ( shouldPrint )
            {
                printDictionaries();
            }
        }

        // Used to print out values stored in the dictionaries to see
        // if 'InitDictionaries()' works as intended
        private void printDictionaries()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("DIdToDv:");
            foreach (int key in DIdToDv.Keys)
            {
                Console.WriteLine
                (
                    "dId:  {0},  Dv:  {1}, ({2}, {3}), {4}, {5}", 
                    key, 
                    DIdToDv[key].DropletId,
                    DIdToDv[key].Coordinate.X,
                    DIdToDv[key].Coordinate.Y,
                    DIdToDv[key].Volume,
                    DIdToDv[key].Colour
                );
            }
            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("OIdToOv:");
            foreach (int key in OIdToOv.Keys)
            {
                Console.WriteLine
                (
                    "oId:  {0},  Ov:  {1}, {2}, {3}, {4}, {5}", 
                    key, 
                    OIdToOv[key].O,
                    OIdToOv[key].M,
                    OIdToOv[key].BigT,
                    OIdToOv[key].LittleT,
                    OIdToOv[key].P
                );
            }
            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("DIdToOIdOut:");
            foreach (int key in DIdToOIdOut.Keys)
            {
                Console.WriteLine
                (
                    "dId:  {0}",
                    key
                );
                foreach (int oId in DIdToOIdOut[key])
                {
                    Console.WriteLine
                    (
                        "oId:  {0}",
                        oId
                    );
                }
                Console.WriteLine(" ");
            }
            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("OIdToDIdOut:");
            foreach (int key in OIdToDIdOut.Keys)
            {
                Console.WriteLine
                (
                    "oId:  {0}",
                    key
                );
                foreach (int dId in OIdToDIdOut[key])
                {
                    Console.WriteLine
                    (
                        "dId:  {0}",
                        dId
                    );
                }
                Console.WriteLine(" ");
            }
            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("DIdToOIdIn:");
            foreach (int key in DIdToOIdIn.Keys)
            {
                Console.WriteLine
                (
                    "dId:  {0}",
                    key
                );
                foreach (int oId in DIdToOIdIn[key])
                {
                    Console.WriteLine
                    (
                        "oId:  {0}",
                        oId
                    );
                }
                Console.WriteLine(" ");
            }
            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("OIdToDIdIn:");
            foreach (int key in OIdToDIdIn.Keys)
            {
                Console.WriteLine
                (
                    "oId:  {0}",
                    key
                );
                foreach (int dId in OIdToDIdIn[key])
                {
                    Console.WriteLine
                    (
                        "dId:  {0}",
                        dId
                    );
                }
                Console.WriteLine(" ");
            }
            Console.WriteLine("\n \n");
        }

        // Initializes a droplet vertex from the given input
        // "d, u, x, c"
        private DropletVertex InitDropletVertex(string dropletString)
        {
            string[] split = dropletString.Split(", ");
            if (split.Count() != 4)
            {
                var errorMessage = String.Format
                (
                    "Droplet vertex does not contain exactly three commas: {0}",
                    dropletString
                );
                throw new InputNotFeasibleException(errorMessage);
            }
            return new DropletVertex(split[0], split[1], split[2], split[3]);
        }


        // Initializes an operation vertex from the given input
        // "o, m, T, t, p"
        private OperationVertex InitOperationVertex(String operationString)
        {
            string[] split = operationString.Split(", ");
            if (split.Count() != 5)
            {
                var errorMessage = String.Format
                (
                    "Operation vertex does not contain exactly four commas: {0}",
                    operationString
                );
                throw new InputNotFeasibleException(errorMessage); 
            }
            return new OperationVertex(split[0], split[1], split[2], split[3], split[4]);
        }
        
        
        // Initializes OIdToOperations
        private void InitOperations()
        {
            foreach (int oId in this.I_o)
            {
                List<DropletVertex> dvsIn;
                dvsIn =
                (
                    from dIdIn in this.OIdToDIdIn[oId]
                    select this.DIdToDv[dIdIn]
                ).ToList();
                OperationVertex ov = this.OIdToOv[oId];
                List<DropletVertex> dvsOut;
                dvsOut =
                (
                    from dIdOut in this.OIdToDIdOut[oId]
                    select this.DIdToDv[dIdOut]
                ).ToList();
                this.OIdToOperations.Add(oId, new Operation( dvsIn, ov, dvsOut ));
            }
            if ( shouldPrint )
            {
                PrintOperations();
            }
        }

        // Prints 'OIdToOperations' when it has been initialized by
        // 'InitOperations()'
        private void PrintOperations()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("OIdToOperations:");
            foreach (int key in OIdToOperations.Keys)
            {
                Console.WriteLine
                (
                    "oId:  {0}",
                    key
                );
                Operation op = OIdToOperations[key];
                var oIdsIn = "{  ";
                foreach (DropletVertex dIn in op.DvsIn)
                {  
                    oIdsIn 
                    += "("
                    + dIn.DropletId.ToString()
                    + ", ("
                    + dIn.Coordinate.X
                    + ", "
                    + dIn.Coordinate.Y
                    + "), "
                    + dIn.Volume
                    + ", "
                    + dIn.Colour
                    + ")  ";     
                }
                oIdsIn += "} -> ";

                var ov
                = "("
                + op.Ov.O
                + ", "
                + op.Ov.M
                + ", "
                + op.Ov.BigT
                + ", "
                + op.Ov.LittleT
                + ", "
                + op.Ov.P
                + ") -> ";

                var oIdsOut = "{  ";
                foreach (DropletVertex dOut in op.DvsOut)
                {  
                    oIdsOut 
                    += "("
                    + dOut.DropletId.ToString()
                    + ", ("
                    + dOut.Coordinate.X
                    + ", "
                    + dOut.Coordinate.Y
                    + "), "
                    + dOut.Volume
                    + ", "
                    + dOut.Colour
                    + ")  ";     
                }
                oIdsOut += "}";
                Console.WriteLine( oIdsIn + ov + oIdsOut );
                Console.WriteLine(" ");
            }
            Console.WriteLine("\n \n");
        }
        public List<Operation> GetFirstOperations()
        {
            //var FirstOperations = new List<Operation>();
            return 
            (
                from operation in OIdToOperations.Values
                where !operation.DvsIn.Select( dv => Roots.Contains(dv) ).Contains(false)
                select operation
            ).ToList();
        }

// ---------------------------------------------------------------------------
// FEASIBILITY CHECKS FOR THE INPUT

        private bool InputNotFeasible(out string errorMessage)
        {
            errorMessage = "";
            if (InitialPlacementsInfeasible(ref errorMessage))
            {
                return true;
            }
            if (OperationsInfeasible(ref errorMessage))
            {
                return true;
            }
            return false;
        }

        // Checks whether all droplets are placed in feasible
        // initial positions
        private bool InitialPlacementsInfeasible(ref string errorMessage)
        {
            // Holds the radius of a droplet
            int radius; 
            // Holds respectively leftmost and rightmost x-coordinates 
            // that a droplet occupies
            int left;
            int right;
            // Holds respectively uppermost and lowermost y-coordinates 
            // that a droplet occupies
            int up;
            int down;
            // We check that the placement of every droplet is correct
            // from the beginning
            foreach (DropletVertex dv in Roots)
            {
                radius = ComputeRadius(dv.Volume);
                left = dv.Coordinate.X - radius + 1;
                right = dv.Coordinate.X + radius - 1;
                down = dv.Coordinate.Y - radius + 1;
                up = dv.Coordinate.Y + radius - 1;
                if ( left < 0 || right > 19 || down < 0 || up > 31 )
                {
                    errorMessage = String.Format
                    (
                        "The initial placement of droplet {0} is infeasible.",
                        dv.DropletId
                    );
                    return true;
                }
            }      
            return false;
        }

        // Checks whether all operations are feasible
        private bool OperationsInfeasible(ref string errorMessage)
        {
            foreach (Operation operation in OIdToOperations.Values)
            {
                switch(operation.Ov.M)
                {
                    case 1:
                        if (MoveOperationInfeasible(operation, ref errorMessage))
                        {
                            return true;
                        }
                        break;
                    case 2:
                        if (MergeOperationInfeasible(operation, ref errorMessage))
                        {
                            return true;
                        }
                        break;
                    case 3:
                        if (MixOperationInfeasible(operation, ref errorMessage))
                        {
                            return true;
                        }
                        break;
                    case 4:
                        if (SplitOperationInfeasible(operation, ref errorMessage))
                        {
                            return true;
                        }
                        break;
                    case 5:
                        if (DispenseOperationInfeasible(operation, ref errorMessage))
                        {
                            return true;
                        }
                        break;
                    case 6:
                        if (HeatOperationInfeasible(operation, ref errorMessage))
                        {
                            return true;
                        }
                        break;
                }
            }
            return false;
        }

        // Checks whether a 'Move' operation is feasible
        private bool MoveOperationInfeasible(Operation operation, ref string errorMessage)
        {
            // Holds the operation vertex
            OperationVertex ov = operation.Ov;
            // Make sure that the operation vertex has exactly one droplet in-neighbour
            if (operation.DvsIn.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 in-neighbour",
                    ov.O
                );
                return true;
            }
            // Make sure that the operation vertex has exactly one droplet out-neighbour
            if (operation.DvsOut.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 out-neighbour",
                    ov.O
                );
                return true;
            }
            // Holds respectively the in- and out-neighbour droplet vertices
            DropletVertex inDv = operation.DvsIn[0];
            DropletVertex outDv = operation.DvsOut[0];
            // Position of outDv must be instantiated
            if (outDv.Coordinate.Uninstantiated)
            {
                errorMessage = String.Format
                (
                    "Position of droplet {0} resulting from 'Move' action is uninstantiated.",
                    outDv.DropletId
                );
                return true;
            }
            // Position of outDv must be inside board
            int radius = ComputeRadius(inDv.Volume);
            int left = outDv.Coordinate.X - radius + 1;
            int right = outDv.Coordinate.X + radius - 1;
            int down = outDv.Coordinate.Y - radius + 1;
            int up = outDv.Coordinate.Y + radius - 1;
            if ( left < 0 || right > 19 || down < 0 || up > 31 )
            {
                errorMessage = String.Format
                (
                    "Position of droplet {0} resulting from 'Move' action is not inside board.",
                    outDv.DropletId
                );
                return true;
            }
            // inDv and outDv should have the same volumes
            if (inDv.Volume != outDv.Volume)
            {
                errorMessage = String.Format
                (
                    "Volume of droplet {0} changed from the volume of droplet {1} during 'Move' action.",
                    outDv.DropletId,
                    inDv.DropletId
                );
                return true;
            }
            // inDv and outDv should have different DropletIds
            if (inDv.DropletId == outDv.DropletId)
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    outDv.DropletId
                );
                return true;
            }
            return false;
        }

        // Checks whether a 'Merge' operation is feasible
        private bool MergeOperationInfeasible(Operation operation, ref string errorMessage)
        {
            // Holds the operation vertex
            OperationVertex ov = operation.Ov;
            // Make sure that the operation vertex has exactly two droplet in-neighbour
            if (operation.DvsIn.Count() != 2)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 2 in-neighbours",
                    ov.O
                );
                return true;
            }
            // Make sure that the operation vertex has exactly one droplet out-neighbour
            if (operation.DvsOut.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 out-neighbour",
                    ov.O
                );
                return true;
            }
            // Holds respectively the in- and out-neighbour droplet vertices
            DropletVertex inDvFirst = operation.DvsIn[0];
            DropletVertex inDvSecond = operation.DvsIn[1];
            DropletVertex outDv = operation.DvsOut[0];
            // outDv.Volume should equal inDvFirst.Volume + inDvSecond.Volume 
            if (outDv.Volume != inDvFirst.Volume + inDvSecond.Volume)
            {
                errorMessage = String.Format
                (
                    "The volume of droplet {0} must equal the sum of the volumes of droplet {1} and {2}",
                    outDv.DropletId,
                    inDvFirst.DropletId,
                    inDvSecond.DropletId
                );
                return true;
            }
            // inDvFirst, inDvSecond and outDv should have different DropletIds
            if 
            (
                inDvFirst.DropletId == inDvSecond.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    inDvFirst.DropletId
                );
                return true;
            }
            if 
            (
                inDvFirst.DropletId == outDv.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    outDv.DropletId
                );
                return true;
            }
            if 
            (
                inDvSecond.DropletId == outDv.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    outDv.DropletId
                );
                return true;
            }
            return false;
        }

        // Checks whether a 'Mix' operation is feasible
        private bool MixOperationInfeasible(Operation operation, ref string errorMessage)
        {
            // Holds the operation vertex
            OperationVertex ov = operation.Ov;
            // Make sure that the operation vertex has exactly one droplet in-neighbour
            if (operation.DvsIn.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 in-neighbour",
                    ov.O
                );
                return true;
            }
            // Make sure that the operation vertex has exactly one droplet out-neighbour
            if (operation.DvsOut.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 out-neighbour",
                    ov.O
                );
                return true;
            }
            // Holds respectively the in- and out-neighbour droplet vertices
            DropletVertex inDv = operation.DvsIn[0];
            DropletVertex outDv = operation.DvsOut[0];
            // inDv and outDv should have the same volumes
            if (inDv.Volume != outDv.Volume)
            {
                errorMessage = String.Format
                (
                    "Volume of droplet {0} changed from the volume of droplet {1} during 'Move' action.",
                    outDv.DropletId,
                    inDv.DropletId
                );
                return true;
            }
            // inDv and outDv should have different DropletIds
            if (inDv.DropletId == outDv.DropletId)
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    outDv.DropletId
                );
                return true;
            }
            return false;
        }

        // Checks whether a 'Split' operation is feasible
        private bool SplitOperationInfeasible(Operation operation, ref string errorMessage)
        {
            // Holds the operation vertex
            OperationVertex ov = operation.Ov;
            // Make sure that the operation vertex has exactly one droplet in-neighbour
            if (operation.DvsIn.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 in-neighbour",
                    ov.O
                );
                return true;
            }
            // Make sure that the operation vertex has exactly two droplet out-neighbours
            if (operation.DvsOut.Count() != 2)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 2 out-neighbours",
                    ov.O
                );
                return true;
            }
            // Holds respectively the in- and out-neighbour droplet vertices
            DropletVertex inDv = operation.DvsIn[0];
            DropletVertex outDvFirst = operation.DvsOut[0];
            DropletVertex outDvSecond = operation.DvsOut[1];
            // outDvFirst.Volume + outDvSecond.Volume should equal inDv.Volume
            if (inDv.Volume != outDvFirst.Volume + outDvSecond.Volume)
            {
                errorMessage = String.Format
                (
                    "The volume of droplet {0} must equal the sum of the volumes of droplet {1} and {2}",
                    inDv.DropletId,
                    outDvFirst.DropletId,
                    outDvSecond.DropletId
                );
                return true;
            }
            // outDvFirst, outDvSecond and inDv should have different DropletIds
            if 
            (
                outDvFirst.DropletId == outDvSecond.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    outDvFirst.DropletId
                );
                return true;
            }
            if 
            (
                outDvFirst.DropletId == inDv.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    inDv.DropletId
                );
                return true;
            }
            if 
            (
                outDvSecond.DropletId == inDv.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    inDv.DropletId
                );
                return true;
            }
            // Make sure that the resulting droplets have the same colour class
            // as the droplet they split from
            if (inDv.Colour != outDvFirst.Colour || inDv.Colour != outDvSecond.Colour)
            {
                errorMessage = String.Format
                (
                    "Droplets {0} and {1} and {2} must share colour class due to split",
                    inDv.DropletId, 
                    outDvFirst.DropletId, 
                    outDvSecond.DropletId
                );
                return true;
            }
            return false;
        }

        // Checks whether a 'Dispense' operation is feasible
        private bool DispenseOperationInfeasible(Operation operation, ref string errorMessage)
        {
            // Holds the operation vertex
            OperationVertex ov = operation.Ov;
            // Make sure that the operation vertex has exactly one droplet in-neighbour
            if (operation.DvsIn.Count() != 1)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 1 in-neighbour",
                    ov.O
                );
                return true;
            }
            // Make sure that the operation vertex has exactly two droplet out-neighbours
            if (operation.DvsOut.Count() != 2)
            {
                errorMessage = String.Format
                (
                    "Operation vertex {0} must have exactly 2 out-neighbours",
                    ov.O
                );
                return true;
            }
            // Holds respectively the in- and out-neighbour droplet vertices
            DropletVertex inDv = operation.DvsIn[0];
            DropletVertex outDvFirst = operation.DvsOut[0];
            DropletVertex outDvSecond = operation.DvsOut[1];
            // outDvFirst.Volume + outDvSecond.Volume should equal inDv.Volume
            if (inDv.Volume != outDvFirst.Volume + outDvSecond.Volume)
            {
                errorMessage = String.Format
                (
                    "The volume of droplet {0} must equal the sum of the volumes of droplet {1} and {2}",
                    inDv.DropletId,
                    outDvFirst.DropletId,
                    outDvSecond.DropletId
                );
                return true;
            }
            // outDvFirst, outDvSecond and inDv should have different DropletIds
            if 
            (
                outDvFirst.DropletId == outDvSecond.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    outDvFirst.DropletId
                );
                return true;
            }
            if 
            (
                outDvFirst.DropletId == inDv.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    inDv.DropletId
                );
                return true;
            }
            if 
            (
                outDvSecond.DropletId == inDv.DropletId
            )
            {
                errorMessage = String.Format
                (
                    "Two droplet vertices exist with droplet id {0}",
                    inDv.DropletId
                );
                return true;
            }
            // Make sure that the resulting droplets have the same colour class
            // as the droplet they split from
            if (inDv.Colour != outDvFirst.Colour || inDv.Colour != outDvSecond.Colour)
            {
                errorMessage = String.Format
                (
                    "Droplets {0} and {1} and {2} must share colour class due to split",
                    inDv.DropletId, 
                    outDvFirst.DropletId, 
                    outDvSecond.DropletId
                );
                return true;
            }
            return false;
        }

        // Checks whether a 'Heat' operation is feasible
        private bool HeatOperationInfeasible(Operation operation, ref string errorMessage)
        {
            // TODO
            return false;
        }


// ---------------------------------------------------------------------------
// HELPING FUNCTIONS

        // Translates the volume of a droplet into a radius of 
        // the board cells it occupies.
        public int ComputeRadius(double volume)
        {
            if ( volume < 2.3 )
            {
                return 1;
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return 2;
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return 3;
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return 3;
        }
    }
}