using System;

namespace DMFRouteSchedule
{
    class Schedule
    {
        // Every 'Dictionary<int, Coordinate>' represents a collection
        // of droplets and their placements on the board at a given timestep,  
        // and the indexes of 'List<...>' correspond
        // to the timesteps of the same number
        List<Dictionary<int, Coordinate>> TimeTable { get; set; }
        
        /*
        // Every 'List<List<int>>' represents the droplet ids of
        // the droplets currently occupying
        // the board at a given timestep corresponding to the
        // index in 'List<...>'
        List<List<List<int?>>> BoardStates { get; set; }

        // Every 'List<List<int>>' represents the class ids of
        // the droplets that have polluted 
        // the board at a given timestep corresponding to the
        // index in 'List<...>'
        List<List<List<int?>>> CGs { get; set; }
        */
        public Schedule()
        {
            TimeTable = new List<Dictionary<int, Coordinate>>();
        }

        public void AddTimestep( Dictionary<int, Coordinate> timeStep )
        {
            TimeTable.Add( timeStep );
        }


    }

}