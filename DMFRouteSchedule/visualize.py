import cairo
import os
import random
import sys



# If True then prints valuable info
should_print = False

##########################################################################
### Directories
##########################################################################
root_directory = sys.argv[1]
colour_directory = root_directory + "droplet-colour-output/python-format/"
id_directory = root_directory + "droplet-id-output/python-format/"


##########################################################################
### For printing a reference
##########################################################################

# Draws a picture relating a key in 'keys' to a colour in 'colours'
# for reference
def _make_reference(keys, colours, directory, name):
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 2048, 2048)
    ctx = cairo.Context(surface)
    ctx.scale(2048, 2048)
    ctx.set_source_rgb(1, 1, 1)
    ctx.rectangle(0, 0, 1, 1)
    ctx.fill()
    
    ctx.select_font_face("Arial", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

    key_x = 0.1
    colour_x = 0.5

    item_count = len(keys)
    step = 1/item_count

    ctx.set_font_size(step)

    for i in range(item_count):
        key = keys[i]
        colour = colours[key]
        ctx.set_source_rgb(0, 0, 0)
        ctx.move_to(key_x, 0+(i+1)*step)
        ctx.show_text(str(key))
        ctx.set_source_rgb(colour[0], colour[1], colour[2])
        ctx.rectangle(colour_x, 0+i*step, 0.25, step)
        ctx.fill()
    
    surface.write_to_png(directory + name + ".png")

##########################################################################
### Colour grid writer
##########################################################################

class Colour_writer:

    def __init__(self, directory, file_name):
        # Used to randomize colours
        random.seed(10)
        self.directory = directory
        self.lines_split = self._init_attributes(directory, file_name)
        # All cells casted as integers
        self.cells = [[int(c) for c in line] for line in self.lines_split[1:]]
        # Height and width of picture
        self.height = len(self.cells)*25
        self.width = len(self.cells[0])*25
        # Amount of cells in y- resp. x-direction
        self.cells_y = len(self.cells)
        self.cells_x = len(self.cells[0])
        # The height resp. width of cells
        self.h = 1/len(self.cells)
        self.w = 1/len(self.cells[1])
        # Colour classes casted as integers. First we trim the list of
        # colours so it only contains unique classes
        self.c_classes = [int(c) for c in self.lines_split[0]]
        self.colours = [ (1, 1, 1) if c == 0 else (random.random(), random.random(), random.random()) for c in self.c_classes]
        if should_print:
            print(self.cells)

    ##########################################################################
    ### Open, read and external data initialization
    ##########################################################################

    def _init_attributes(self, directory, file_name):
        with open(directory + file_name) as f:
            lines = f.readlines()
            lines_split = [line.split() for line in lines]
            return lines_split

    ##########################################################################
    ### Cairo
    ##########################################################################

    def _make_surface(self):
        # Initialization
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.width, self.height)
        ctx = cairo.Context(surface)
        ctx.scale(self.width, self.height)
        ctx.set_line_width(0.01)

        # Colour the cells and draw the lines
        for i in range(self.cells_y):
            # Vertical lines
            ctx.set_source_rgb(0, 0, 0)
            ctx.move_to(0, 0+i*self.h)
            ctx.line_to(1, 0+i*self.h)
            ctx.stroke()
            for j in range(self.cells_x):
                # Horizontal lines
                ctx.set_source_rgb(0, 0, 0)
                ctx.move_to(0+j*self.w, 0)
                ctx.line_to(0+j*self.w, 1)
                ctx.stroke()
                # Cell colouring
                ctx.rectangle(0+j*self.w, 0+i*self.h, self.w, self.h)
                colour = self.cells[i][j]
                (r, g, b) = self.colours[colour]
                ctx.set_source_rgb(r, g, b)
                ctx.fill()
        return surface

    ##########################################################################
    ### Print
    ##########################################################################     
    
    def write_reference(self):
        out_directory = root_directory + "droplet-colour-output/pictures/"
        _make_reference(self.c_classes, self.colours, out_directory)

    def write_image(self, file_name):
        out_directory = root_directory + "droplet-colour-output/pictures/"
        surface = self._make_surface()
        file_number = file_name[:-4]
        surface.write_to_png(out_directory+file_number+".png")



##########################################################################
### Droplet id writer
##########################################################################

class Id_writer:

    def __init__(self, directory, file_name):
        # Used to randomize id colours
        random.seed(15)

        self.directory = directory
        self.lines_split = self._init_attributes(directory, file_name)
        # All cells casted as integers
        self.cells = [[int(c) if c != "x" else None for c in line] for line in self.lines_split[1:]]
        # Height and width of picture
        self.height = len(self.cells)*25
        self.width = len(self.cells[0])*25
        # Amount of cells in y- resp. x-direction
        self.cells_y = len(self.cells)
        self.cells_x = len(self.cells[0])
        # The height resp. width of cells
        self.h = 1/len(self.cells)
        self.w = 1/len(self.cells[1])
        # Ids casted as integers and 
        self.ids = [int(id) for id in self.lines_split[0]]
        # Colours used for drawing lines between adjacent cells
        # based on their cell content
        self.colours =  {
            (None, i) : (random.random(), random.random(), random.random())
            for i in range(len(self.ids))
        }
        for (i, j) in [key for key in self.colours.keys()]:
            self.colours[(j, i)] = self.colours[(i, j)]

        if should_print:
            print(self.cells)
            print(self.colours)

    ##########################################################################
    ### Open, read and external data initialization
    ##########################################################################

    def _init_attributes(self, directory, file_name):
        with open(directory + file_name) as f:
            lines = f.readlines()
            lines_split = [line.split() for line in lines]
            return lines_split

    ##########################################################################
    ### Cairo
    ##########################################################################

    def _make_surface(self):
        # Initialization
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.width, self.height)
        ctx = cairo.Context(surface)
        ctx.scale(self.width, self.height)
        ctx.set_line_width(0.01)
        # Colour the canvas white
        ctx.set_source_rgb(1, 1, 1)
        ctx.rectangle(0, 0, 1, 1)
        ctx.fill()

        for I in range(1, self.cells_y):
            i = I-1

            for J in range(1, self.cells_x):
                j = J-1

                # Vertical lines:
                c_J = self.cells[i][J]
                c_j = self.cells[i][j]
                colour_vertical = self.colours.get((c_j, c_J))
                if colour_vertical != None:
                    ctx.set_source_rgb(colour_vertical[0], colour_vertical[1], colour_vertical[2])
                else:
                    ctx.set_source_rgb(0.7, 0.7, 0.7)
                ctx.move_to(0+J*self.w, 0+i*self.h)
                ctx.line_to(0+J*self.w, 0+I*self.h)
                ctx.stroke()

                # Horizontal lines:
                c_I = self.cells[I][j]
                c_i = self.cells[i][j]
                colour = self.colours.get((c_i, c_I))
                if colour != None:
                    ctx.set_source_rgb(colour[0], colour[1], colour[2])
                else:
                    ctx.set_source_rgb(0.7, 0.7, 0.7)
                ctx.move_to(0+j*self.w, 0+I*self.h)
                ctx.line_to(0+J*self.w, 0+I*self.h)
                ctx.stroke()

        # Rightmost column of horizontal lines
        j_max = self.cells_x - 1
        for I in range(1, self.cells_y):
            c_I = self.cells[I][j_max]
            c_i = self.cells[I-1][j_max]
            colour = self.colours.get((c_i, c_I))
            if colour != None:
                    ctx.set_source_rgb(colour[0], colour[1], colour[2])
            else:
                ctx.set_source_rgb(0.7, 0.7, 0.7)
            ctx.move_to(1-self.w, 0+I*self.h)
            ctx.line_to(1, 0+I*self.h)
            ctx.stroke()

        # Lowest row of vertical lines
        i_max = self.cells_y - 1
        for J in range(1, self.cells_x):
            c_J = self.cells[i_max][J]
            c_j = self.cells[i_max][J-1]
            colour = self.colours.get((c_j, c_J))
            if colour != None:
                    ctx.set_source_rgb(colour[0], colour[1], colour[2])
            else:
                ctx.set_source_rgb(0.7, 0.7, 0.7)
            ctx.move_to(0+J*self.w, 1-self.h)
            ctx.line_to(0+J*self.w, 1)
            ctx.stroke()

        return surface

    ##########################################################################
    ### Print
    ##########################################################################     
    
    def write_reference(self):
        out_directory = root_directory + "droplet-id-output/pictures/"
        colours = [self.colours[(key, None)] for key in self.ids]
        _make_reference(self.ids, colours, out_directory)

    def write_image(self, file_name):
        out_directory = root_directory + "droplet-id-output/pictures/"
        surface = self._make_surface()
        file_number = file_name[:-4]
        surface.write_to_png(out_directory+file_number+".png")

##########################################################################
### Colour grid and droplet id writer
##########################################################################

class Colour_and_id_writer:

    def __init__(self, colour_directory, id_directory, file_name):
        ### INIT POLLUTION INPUT
        # Used to randomize pollution colours
        random.seed(10)
        self.colour_directory = colour_directory
        self.colour_lines_split = self._init_attributes(colour_directory, file_name)
        # All cells casted as integers
        self.colour_cells = [[int(c) for c in line] for line in self.colour_lines_split[1:]]
        # Colour classes casted as integers. First we trim the list of
        # colours so it only contains unique classes.
        # 'c_classes' and 'c_colours' are the lists of classes and colours belonging to the 
        # pollution colours
        self.c_classes = [int(c) for c in self.colour_lines_split[0]]
        self.c_colours = [ (1, 1, 1) if c == 0 else (random.random(), random.random(), random.random()) for c in self.c_classes]
        ### INIT ID INPUT
        # Used to randomize id colours
        random.seed(15)
        self.id_directory = id_directory
        self.id_lines_split = self._init_attributes(id_directory, file_name)
        # All cells casted as integers
        self.id_cells = [[int(c) if c != "x" else None for c in line] for line in self.id_lines_split[1:]]
        # Ids casted as integers and 
        self.ids = [int(id) for id in self.id_lines_split[0]]
        # Colours used for drawing lines between adjacent cells
        # based on their cell content
        self.id_colours =  {
            (None, i) : (random.random(), random.random(), random.random())
            for i in range(len(self.ids))
        }
        for (i, j) in [key for key in self.id_colours.keys()]:
            self.id_colours[(j, i)] = self.id_colours[(i, j)]
        
        # Height and width of picture
        self.height = len(self.colour_cells)*25
        self.width = len(self.colour_cells[0])*25
        # Amount of cells in y- resp. x-direction
        self.cells_y = len(self.colour_cells)
        self.cells_x = len(self.colour_cells[0])
        # The height resp. width of cells
        self.h = 1/len(self.colour_cells)
        self.w = 1/len(self.colour_cells[1])
        
        if should_print:
            print(self.colour_cells)
            print(self.id_cells)
            print(self.id_colours)

    ##########################################################################
    ### Open, read and external data initialization
    ##########################################################################

    def _init_attributes(self, directory, file_name):
        with open(directory + file_name) as f:
            lines = f.readlines()
            lines_split = [line.split() for line in lines]
            return lines_split

    ##########################################################################
    ### Cairo
    ##########################################################################

    def _make_surface(self):
        # Initialization
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.width, self.height)
        ctx = cairo.Context(surface)
        ctx.scale(self.width, self.height)
        ctx.set_line_width(0.01)

        for I in range(1, self.cells_y):
            i = I-1

            for J in range(1, self.cells_x):
                j = J-1

                # Cell colouring
                ctx.rectangle(0+j*self.w, 0+i*self.h, self.w, self.h)
                colour = self.colour_cells[i][j]
                (r, g, b) = self.c_colours[colour]
                ctx.set_source_rgb(r, g, b)
                ctx.fill()

                # Vertical lines:
                c_J = self.id_cells[i][J]
                c_j = self.id_cells[i][j]
                colour_vertical = self.id_colours.get((c_j, c_J))
                if colour_vertical != None:
                    ctx.set_source_rgb(colour_vertical[0], colour_vertical[1], colour_vertical[2])
                else:
                    ctx.set_source_rgb(0.7, 0.7, 0.7)
                ctx.move_to(0+J*self.w, 0+i*self.h)
                ctx.line_to(0+J*self.w, 0+I*self.h)
                ctx.stroke()

                # Horizontal lines:
                c_I = self.id_cells[I][j]
                c_i = self.id_cells[i][j]
                colour = self.id_colours.get((c_i, c_I))
                if colour != None:
                    ctx.set_source_rgb(colour[0], colour[1], colour[2])
                else:
                    ctx.set_source_rgb(0.7, 0.7, 0.7)
                ctx.move_to(0+j*self.w, 0+I*self.h)
                ctx.line_to(0+J*self.w, 0+I*self.h)
                ctx.stroke()


        # Rightmost column of horizontal lines
        j_max = self.cells_x - 1
        for I in range(1, self.cells_y):
            # Colour cells
            i = I - 1
            ctx.rectangle(1-self.w, 0+i*self.h, self.w, self.h)
            colour = self.colour_cells[i][j_max]
            (r, g, b) = self.c_colours[colour]
            ctx.set_source_rgb(r, g, b)
            ctx.fill()
            # draw lines
            c_I = self.id_cells[I][j_max]
            c_i = self.id_cells[I-1][j_max]
            colour = self.id_colours.get((c_i, c_I))
            if colour != None:
                    ctx.set_source_rgb(colour[0], colour[1], colour[2])
            else:
                ctx.set_source_rgb(0.7, 0.7, 0.7)
            ctx.move_to(1-self.w, 0+I*self.h)
            ctx.line_to(1, 0+I*self.h)
            ctx.stroke()

        # Lowest row of vertical lines
        i_max = self.cells_y - 1
        for J in range(1, self.cells_x):
            # Colour cells
            j = J - 1
            ctx.rectangle(0+j*self.w, 1-self.h, self.w, self.h)
            colour = self.colour_cells[i_max][j]
            (r, g, b) = self.c_colours[colour]
            ctx.set_source_rgb(r, g, b)
            ctx.fill()
            # Draw lines
            c_J = self.id_cells[i_max][J]
            c_j = self.id_cells[i_max][J-1]
            colour = self.id_colours.get((c_j, c_J))
            if colour != None:
                    ctx.set_source_rgb(colour[0], colour[1], colour[2])
            else:
                ctx.set_source_rgb(0.7, 0.7, 0.7)
            ctx.move_to(0+J*self.w, 1-self.h)
            ctx.line_to(0+J*self.w, 1)
            ctx.stroke()
            
        # Colour last cell in the lowest row in the rightmost column
        ctx.rectangle(1-self.w, 1-self.h, self.w, self.h)
        colour = self.colour_cells[i_max][j_max]
        (r, g, b) = self.c_colours[colour]
        ctx.set_source_rgb(r, g, b)
        ctx.fill()

        return surface

    ##########################################################################
    ### Print
    ##########################################################################     
    
    def write_reference(self):
        out_directory = root_directory + "droplet-colour-and-id-output/pictures/"
        _make_reference(self.c_classes, self.c_colours, out_directory, "colour_reference")

        colours = [self.id_colours[(key, None)] for key in self.ids]
        _make_reference(self.ids, colours, out_directory, "id_reference")

    def write_image(self, file_name):
        out_directory = root_directory + "droplet-colour-and-id-output/pictures/"
        surface = self._make_surface()
        file_number = file_name[:-4]
        surface.write_to_png(out_directory+file_number+".png")



##########################################################################
### File names
##########################################################################

def all_files(path):
    _, _, file_names = os.walk(path).__next__()
    return file_names


##########################################################################
### Main
##########################################################################

# WE ASSUME 'colour_directory' and 'id_directory' contain corresponding
# .txt-format files of equal names

colour_files = all_files(colour_directory)
# Colour_writer(colour_directory, "1.txt").write_reference()
# for file_name in colour_files:
#     cw = Colour_writer(colour_directory, file_name)
#     cw.write_image(file_name)

id_files = all_files(id_directory)
# Id_writer(id_directory, "1.txt").write_reference()
# for file_name in id_files:
#     iw = Id_writer(id_directory, file_name)
#     iw.write_image(file_name)

files = all_files(id_directory)
Colour_and_id_writer(colour_directory, id_directory, "1.txt").write_reference()
for file_name in files:
    c_and_i_writer = Colour_and_id_writer(colour_directory, id_directory, file_name)
    c_and_i_writer.write_image(file_name)

