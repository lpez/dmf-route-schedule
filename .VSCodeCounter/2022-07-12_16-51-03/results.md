# Summary

Date : 2022-07-12 16:51:03

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  3915 codes, 883 comments, 312 blanks, all 5110 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 11 | 3,720 | 816 | 254 | 4,790 |
| Python | 1 | 184 | 67 | 58 | 309 |
| XML | 1 | 11 | 0 | 0 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 13 | 3,915 | 883 | 312 | 5,110 |
| DMFRouteSchedule | 13 | 3,915 | 883 | 312 | 5,110 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)