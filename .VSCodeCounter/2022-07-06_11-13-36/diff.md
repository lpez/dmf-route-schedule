# Diff Summary

Date : 2022-07-06 11:13:36

Directory /home/kjoh/dmf-route-schedule

Total : 5 files,  198 codes, 37 comments, 12 blanks, all 247 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 5 | 198 | 37 | 12 | 247 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 5 | 198 | 37 | 12 | 247 |
| DMFRouteSchedule | 5 | 198 | 37 | 12 | 247 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)