# Summary

Date : 2022-07-06 11:13:36

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  3084 codes, 690 comments, 301 blanks, all 4075 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 11 | 2,889 | 623 | 243 | 3,755 |
| Python | 1 | 184 | 67 | 58 | 309 |
| XML | 1 | 11 | 0 | 0 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 13 | 3,084 | 690 | 301 | 4,075 |
| DMFRouteSchedule | 13 | 3,084 | 690 | 301 | 4,075 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)