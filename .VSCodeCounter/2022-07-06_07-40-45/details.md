# Details

Date : 2022-07-06 07:40:45

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  2886 codes, 653 comments, 289 blanks, all 3828 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/Coordinate.cs](/DMFRouteSchedule/Coordinate.cs) | C# | 33 | 4 | 3 | 40 |
| [DMFRouteSchedule/DMFExceptions.cs](/DMFRouteSchedule/DMFExceptions.cs) | C# | 22 | 0 | 2 | 24 |
| [DMFRouteSchedule/DMFRouteSchedule.csproj](/DMFRouteSchedule/DMFRouteSchedule.csproj) | XML | 11 | 0 | 0 | 11 |
| [DMFRouteSchedule/Execution.cs](/DMFRouteSchedule/Execution.cs) | C# | 1,013 | 311 | 65 | 1,389 |
| [DMFRouteSchedule/Main.cs](/DMFRouteSchedule/Main.cs) | C# | 19 | 17 | 11 | 47 |
| [DMFRouteSchedule/Metrics.cs](/DMFRouteSchedule/Metrics.cs) | C# | 60 | 12 | 9 | 81 |
| [DMFRouteSchedule/OperationGraph.cs](/DMFRouteSchedule/OperationGraph.cs) | C# | 170 | 22 | 18 | 210 |
| [DMFRouteSchedule/OperationGraphGenerator.cs](/DMFRouteSchedule/OperationGraphGenerator.cs) | C# | 193 | 23 | 30 | 246 |
| [DMFRouteSchedule/OperationGraphInitializer.cs](/DMFRouteSchedule/OperationGraphInitializer.cs) | C# | 602 | 59 | 40 | 701 |
| [DMFRouteSchedule/Printer.cs](/DMFRouteSchedule/Printer.cs) | C# | 250 | 26 | 24 | 300 |
| [DMFRouteSchedule/Schedule.cs](/DMFRouteSchedule/Schedule.cs) | C# | 16 | 17 | 6 | 39 |
| [DMFRouteSchedule/Topology.cs](/DMFRouteSchedule/Topology.cs) | C# | 313 | 95 | 23 | 431 |
| [DMFRouteSchedule/visualize.py](/DMFRouteSchedule/visualize.py) | Python | 184 | 67 | 58 | 309 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)