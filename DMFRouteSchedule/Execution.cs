using System;

// TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//
//
//
//
//
//  - Add a guard so percentage moved by an operation
//      corresponds with volumes of in-neighbours and out-neighbours
//
//
//
//
//
//
//
//
//

namespace DMFRouteSchedule
{
    class DropletInAction
    {  
        public DropletVertex DropletVertex { get; set; }
        public OperationVertex OperationVertex { get; set; }
        public Coordinate? TargetCoordinate { get; set; }
        public DropletVertex? TargetDropletVertex { get; set; }
        // When current droplet and 'TargetDropletVertex' are further away
        // from each other than 'CriticalDistance' in either vertical distance or
        // horizontal distance, then we know they do not touch
        public int CriticalDistance { get; }
        // The volume current droplet and 'TargetDropletVertex' attain
        // when they merge
        public double MergeVolume { get; }
        // Represents the grid topology as seen through the eyes
        // of this droplet in action. I.e. used if we want to set
        // invisible walls where this droplet can not pass
        // *TODO WHEN NEEDED*
        public List<List<bool>> LocalTopology { get; set; }
        // The upper bound of steps we can use to finish current action
        public int StepsLeftMaximum { get; set; }
        // A lower bound of steps we must use to finish current action
        // (how to use depends on which action we perform)
        public int StepsLeftMinimum { get; set; }
        // Used to signify the direction going in.
        // Specifically used for the 'Mix' action
        public int Direction { get; set; }
        public Coordinate PreviousPosition { get; set; }

        public DropletInAction( DropletVertex dv, OperationVertex ov,
                                Coordinate? target, DropletVertex? targetDv,
                                int criticalDistance, double mergeVolume,
                                int stepsLeftMaximum, int stepsLeftMinimum )
        {
            DropletVertex = dv;
            OperationVertex = ov;
            TargetCoordinate = target;
            TargetDropletVertex = targetDv;
            CriticalDistance = criticalDistance;
            MergeVolume = mergeVolume;
            StepsLeftMaximum = stepsLeftMaximum;
            StepsLeftMinimum = stepsLeftMinimum;
            Direction = 0;
            PreviousPosition = new Coordinate(null, null);
        }

        // *TODO WHEN NEEDED*
        private void InitLocalTopology()
        {
            if (OperationVertex.M == 1)
            {
                
            }
            if (OperationVertex.M == 2)
            {
                
            }
            /*
            if (OperationVertex.M == 3)
            {
                
            }
            if (OperationVertex.M == 4)
            {
                
            }
            if (OperationVertex.M == 5)
            {
                
            }
            if (OperationVertex.M == 6)
            {
                
            }
            */
        }

        public void InitLocalTopology(List<List<bool>> localTopology)
        {
            LocalTopology = localTopology;
        }
    }

    class Executor 
    {
        // ------------------------------
        // Used for info printing method calls
        private bool shouldPrint = false;
        private int mergeCount = 0;
        private int seed;
        // ------------------------------


        private Random random;
        private OperationGraph og;
        private GridConfiguration gcf;
        public List<List<int>> CG { get; set; }
        private Schedule schedule;
        // Denotes the Board and gives us a pointer from every cell
        // to the droplet vertex that occupies it
        public List<List<DropletVertex?>> Board { get; set; }
        // Contains a reference from every DropletVertex to its corresponding
        // 'DropletInAction' object. 
        // IMPORTANT: Only includes droplets that are supposed to be moved.
        // Thus, no leaves are included
        private Dictionary<DropletVertex, DropletInAction> dropletVertexToDropletInAction;
        private Dictionary<DropletInAction, bool> executionQueue;
        private Printer printer;

        public Executor( OperationGraph og, GridConfiguration gcf, int seed )
        {
            this.seed = seed;
            this.gcf = gcf;
            this.og = og;
            CG = og.CG;
            schedule = new Schedule();
            printer = new Printer(gcf, og, this);
            random = new Random(seed);
            InitBoard();
            InitDropletVertexToDropletInAction();
            InitExecutionQueue();
        }

// ---------------------------------------------------------------------------
// HEURISTICS

        // Quick heuristic using greedy distance measures and simple
        // randomization. Not very effective for complex problems
        public void Heuristic1
        (
            int numberOfIterations, bool shouldPrintText, 
            bool shouldPrintPictures, string outputDirectory
        )
        {
            int iterationNumber = 0;
            // Printing the initial board state
            if (shouldPrintText)
            {
                printer.WriteBoardDropletIds( iterationNumber.ToString(), Board );
                printer.WriteCG( iterationNumber.ToString(), CG );   
            }
            if (shouldPrintPictures)
            {
                printer.WriteBoardDropletIdsPythonFormat( outputDirectory, iterationNumber.ToString(), Board );
                printer.WriteCGPythonFormat( outputDirectory, iterationNumber.ToString(), CG );
            }

            while ( executionQueue.Count is not 0 && iterationNumber < numberOfIterations )
            {
                // If we want to print additional information used for debugging
                if (shouldPrint)
                {
                    PrintExecutionQueue(iterationNumber);
                }
                // Main loop
                foreach ( DropletInAction dropletInAction in executionQueue.Keys.ToList() )
                {
                    if ( dropletInAction.OperationVertex.M is 1 )
                    {
                        if (dropletInAction.StepsLeftMaximum > 0)
                        {
                            PerformMove( dropletInAction );
                        }
                        else
                        {
                            var errorMessage = String.Format
                            (
                                "Could not perform action 'Move' on droplet {0}",
                                dropletInAction.DropletVertex.DropletId
                            );
                            throw new MaximumStepsReachedException(errorMessage);
                        }
                    }
                    else if ( dropletInAction.OperationVertex.M is 2 )
                    {
                        if (dropletInAction.StepsLeftMaximum > 0)
                        {
                            bool _;
                            DropletInAction partnerDroplet = 
                            dropletVertexToDropletInAction [ dropletInAction.TargetDropletVertex ];
                            // Before we move the current droplet towards its partner
                            // we must be sure that its partner has been queued already.
                            if ( executionQueue.TryGetValue( partnerDroplet, out _ ) )
                            {
                                PerformMerge( dropletInAction );
                            }
                        }
                        else
                        {
                            var errorMessage = String.Format
                            (
                                "Could not perform action 'Merge' on droplet {0}",
                                dropletInAction.DropletVertex.DropletId
                            );
                            throw new MaximumStepsReachedException(errorMessage);
                        }
                    }
                    else if ( dropletInAction.OperationVertex.M is 3 )
                    {
                        PerformMix ( dropletInAction );
                    }
                    else if ( dropletInAction.OperationVertex.M is 4 )
                    {
                        PerformSplit( dropletInAction );
                    }
                    else if ( dropletInAction.OperationVertex.M is 5 )
                    {
                        PerformDispense( dropletInAction );
                    }
                }
                iterationNumber++;
                // Prints board states after the iteration
                if (shouldPrintText)
                {
                    printer.WriteBoardDropletIds( iterationNumber.ToString(), Board );
                    printer.WriteCG( iterationNumber.ToString(), CG );   
                }
                if (shouldPrintPictures)
                {
                    printer.WriteBoardDropletIdsPythonFormat( outputDirectory, iterationNumber.ToString(), Board );
                    printer.WriteCGPythonFormat( outputDirectory, iterationNumber.ToString(), CG );
                }
            }
            // Creates pictures
            if (shouldPrintPictures)
            {
                
                printer.WritePicturesPython(outputDirectory);
            }
        }

        // Quick heuristic using greedy distance measures and simple
        // randomization. Not very effective for complex problems
        public void Heuristic2
        (
            int numberOfIterations, bool shouldPrintText, 
            bool shouldPrintPictures, string outputDirectory
        )
        {
            int iterationNumber = 0;
            // Printing the initial board state
            if (shouldPrintText)
            {
                printer.WriteBoardDropletIds( iterationNumber.ToString(), Board );
                printer.WriteCG( iterationNumber.ToString(), CG );   
            }
            if (shouldPrintPictures)
            {
                printer.WriteBoardDropletIdsPythonFormat( outputDirectory, iterationNumber.ToString(), Board );
                printer.WriteCGPythonFormat( outputDirectory, iterationNumber.ToString(), CG );
            }

            while ( executionQueue.Count is not 0 && iterationNumber < numberOfIterations )
            {
                // If we want to print additional information used for debugging
                if (shouldPrint)
                {
                    PrintExecutionQueue(iterationNumber);
                }
                // Main loop
                foreach ( DropletInAction dropletInAction in executionQueue.Keys.ToList() )
                {
                    if ( dropletInAction.OperationVertex.M is 1 )
                    {
                        if (dropletInAction.StepsLeftMaximum > 0)
                        {
                            PerformMove( dropletInAction );
                        }
                        else
                        {
                            var errorMessage = String.Format
                            (
                                "Could not perform action 'Move' on droplet {0}",
                                dropletInAction.DropletVertex.DropletId
                            );
                            throw new MaximumStepsReachedException(errorMessage);
                        }
                    }
                    else if ( dropletInAction.OperationVertex.M is 2 )
                    {
                        if (dropletInAction.StepsLeftMaximum > 0)
                        {
                            bool _;
                            DropletInAction partnerDroplet = 
                            dropletVertexToDropletInAction [ dropletInAction.TargetDropletVertex ];
                            // Before we move the current droplet towards its partner
                            // we must be sure that its partner has been queued already.
                            if ( executionQueue.TryGetValue( partnerDroplet, out _ ) )
                            {
                                PerformMerge( dropletInAction );
                            }
                        }
                        else
                        {
                            var errorMessage = String.Format
                            (
                                "Could not perform action 'Merge' on droplet {0}",
                                dropletInAction.DropletVertex.DropletId
                            );
                            throw new MaximumStepsReachedException(errorMessage);
                        }
                    }
                    else if ( dropletInAction.OperationVertex.M is 3 )
                    {
                        PerformMix ( dropletInAction );
                    }
                    else if ( dropletInAction.OperationVertex.M is 4 )
                    {
                        PerformSplit( dropletInAction );
                    }
                    else if ( dropletInAction.OperationVertex.M is 5 )
                    {
                        PerformDispense( dropletInAction );
                    }
                }
                iterationNumber++;
                // Prints board states after the iteration
                if (shouldPrintText)
                {
                    printer.WriteBoardDropletIds( iterationNumber.ToString(), Board );
                    printer.WriteCG( iterationNumber.ToString(), CG );   
                }
                if (shouldPrintPictures)
                {
                    printer.WriteBoardDropletIdsPythonFormat( outputDirectory, iterationNumber.ToString(), Board );
                    printer.WriteCGPythonFormat( outputDirectory, iterationNumber.ToString(), CG );
                }
            }
            // Creates pictures
            if (shouldPrintPictures)
            {
                
                printer.WritePicturesPython(outputDirectory);
            }
        }

// ---------------------------------------------------------------------------
// MOVE METHODS

        // Performs a 'Move' operation. See description of 'PerformMerge' method
        // and notice the similarities between 'PerformMove' and 'PerformMerge'
        // to realize its functionality.
        private void PerformMove( DropletInAction dIA )
        {
            var target = BestMoveCoordinate( dIA );
            DropletVertex resultingDropletVertex;
            // If target results in a merge we want to update with the
            // resulting droplet.
            if ( target.OnTopOf( dIA.TargetCoordinate ) )
            {
                QueueNextDropletsInAction( dIA );
                DequeueDropletInAction( dIA.DropletVertex );
                resultingDropletVertex = og.OperationVertexToDropletVertex[ dIA.OperationVertex ][0];
            } else
            {
                resultingDropletVertex = dIA.DropletVertex;
                dIA.StepsLeftMaximum--;
            }
            // Old cells and new cells to be occupied
            var oldOccupiedCells = gcf.NX( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume );
            var newOccupiedCells = gcf.NX( target, resultingDropletVertex.Volume );

            // The colour of the droplet we try to move should become
            // a mix of all the colours of the cells it will occupy after 
            // the move
            foreach ( Coordinate coordinate in newOccupiedCells )
            {
                resultingDropletVertex.Colour = og.E
                [ resultingDropletVertex.Colour ] 
                [ CG [ coordinate.X ] [ coordinate.Y ] ];
            }
            // Remove the droplet from the Board cells it currently occupies
            foreach ( Coordinate coordinate in oldOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = null;
            }
            // Move the droplet to the new Board cells and colour these cells with
            // the colour of the droplet
            foreach ( Coordinate coordinate in newOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = resultingDropletVertex;
                CG[coordinate.X][coordinate.Y] 
                = resultingDropletVertex.Colour;
            }
            resultingDropletVertex.Coordinate = target;
        }

// ---------------------------------------------------------------------------
// MERGE METHODS

        // When we use 'PerformMerge' on a droplet vertex we always
        // know the coordinates of the droplet vertex (i.e., the droplet vertex)
        // have been instantiated.
        //
        // Takes a 'DropletInAction' and fcoordinateinds the best coordinate to
        // move the droplet to. In case this move merges the droplet with its
        // partner droplet then itself and its partner turn into a new droplet
        // droplet 'resultingDroplet'. In this case the droplet to move is now 'ResultingDroplet',,
        // and so we must remove the previous two droplets form the queue
        // and add 'desultingDroplet' to the queue.
        private void PerformMerge( DropletInAction dIA )
        {
            var target = BestMergeCoordinate( dIA );
            DropletVertex resultingDropletVertex;
            // If target results in a merge we want to update with the
            // resulting droplet.
            if ( target.OnTopOf( dIA.TargetDropletVertex.Coordinate ) )
            {
                // Dequeue both merging droplet vertices
                QueueNextDropletsInAction( dIA );
                DequeueDropletInAction( dIA.DropletVertex );
                DequeueDropletInAction( dIA.TargetDropletVertex );
                resultingDropletVertex = og.OperationVertexToDropletVertex[ dIA.OperationVertex ][0];
            } else
            {
                resultingDropletVertex = dIA.DropletVertex;
                dIA.StepsLeftMaximum--;
            }
            // Old cells and new cells to be occupied
            var oldOccupiedCells = gcf.NX( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume );
            var newOccupiedCells = gcf.NX( target, resultingDropletVertex.Volume );

            // The colour of the droplet we try to move should become
            // a mix of all the colours of the cells it will occupy after 
            // the move
            foreach ( Coordinate coordinate in newOccupiedCells )
            {
                resultingDropletVertex.Colour = og.E
                [ resultingDropletVertex.Colour ] 
                [ CG [ coordinate.X ] [ coordinate.Y ] ];
            }
            // Remove the droplet from the Board cells it currently occupies
            foreach ( Coordinate coordinate in oldOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = null;
            }
            // Move the droplet to the new Board cells and colour these cells with
            // the colour of the droplet
            foreach ( Coordinate coordinate in newOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = resultingDropletVertex;
                CG[coordinate.X][coordinate.Y] 
                = resultingDropletVertex.Colour;
            }
            resultingDropletVertex.Coordinate = target;

            if (shouldPrint)
            {
                PrintMerge(dIA, target, resultingDropletVertex);
            }
        }

// ---------------------------------------------------------------------------
// MIX METHODS

        // Takes a 'DropletInAction' and performs a mix action.
        // Also queues and dequeues next pair of 'DropletInAction'.
        private void PerformMix( DropletInAction dIA )
        {
            var target = BestMixCoordinate( dIA );
            DropletVertex resultingDropletVertex;
            // Since we update 'dIA.StepsLeftMinimum' in 'BestMixCoordinate (...)'
            // the following condition means the move we're going to make will be the
            // last one necessary, hence we queue and dequeue
            if ( dIA.StepsLeftMinimum <= 0 )
            {
                QueueNextDropletsInAction( dIA );
                DequeueDropletInAction( dIA.DropletVertex );
                resultingDropletVertex = og.OperationVertexToDropletVertex[ dIA.OperationVertex ][0];
            } else
            {
                resultingDropletVertex = dIA.DropletVertex;
                dIA.StepsLeftMaximum--;
            }
            // Old cells and new cells to be occupied
            var oldOccupiedCells = gcf.NX( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume );
            var newOccupiedCells = gcf.NX( target, resultingDropletVertex.Volume );

            // The colour of the droplet we try to move should become
            // a mix of all the colours of the cells it will occupy after 
            // the move
            foreach ( Coordinate coordinate in newOccupiedCells )
            {
                resultingDropletVertex.Colour = og.E
                [ resultingDropletVertex.Colour ] 
                [ CG [ coordinate.X ] [ coordinate.Y ] ];
            }
            // Remove the droplet from the Board cells it currently occupies
            foreach ( Coordinate coordinate in oldOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = null;
            }
            // Move the droplet to the new Board cells and colour these cells with
            // the colour of the droplet
            foreach ( Coordinate coordinate in newOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = resultingDropletVertex;
                CG[coordinate.X][coordinate.Y] 
                = resultingDropletVertex.Colour;
            }
            resultingDropletVertex.Coordinate = target;
        }

// ---------------------------------------------------------------------------
// SPLIT METHODS

        // Takes a 'DropletInAction' and performs a split action.
        // Also queues and dequeues next pair of 'DropletInAction'.
        private void PerformSplit( DropletInAction dIA )
        {
            bool movesAreAvailable = BestSplitCoordinates( dIA, out Dictionary<Coordinate, DropletVertex> moves );
            // If no moves are available:
            if (!movesAreAvailable)
            {
                throw new NoAvailableMoveException("No move available");
            }
            // Coordinates and the respective droplet vertices
            Coordinate outC1 = moves.Keys.ToList()[0];
            DropletVertex outDv1 = moves[outC1];
            Coordinate outC2 = moves.Keys.ToList()[1];
            DropletVertex outDv2 = moves[outC2];
            // Old and new occupied cells
            var oldOccupiedCells = gcf.NX( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume );
            var newOccupiedCells1 = gcf.NX( outC1, outDv1.Volume );
            var newOccupiedCells2 = gcf.NX( outC2, outDv2.Volume );
            // Queue and dequeue every 'DropletInAction'
            QueueNextDropletsInAction( dIA );
            DequeueDropletInAction( dIA.DropletVertex );
            // The sequence of cells that will be traversed by respectively outDv1 and outDv2
            List<Coordinate> sequenceOfCells1 = SequenceOfCells(dIA.DropletVertex.Coordinate, outC1);
            List<Coordinate> sequenceOfCells2 = SequenceOfCells(dIA.DropletVertex.Coordinate, outC2);
            // Remove the droplet that was split from the Board cells it currently occupies
            foreach ( Coordinate coordinate in oldOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = null;
            }
            // Move 'outDv1' to the new Board cells
            foreach ( Coordinate coordinate in newOccupiedCells1 )
            {
                Board[coordinate.X][coordinate.Y] = outDv1;
            }
            outDv1.Coordinate = outC1;
            // Move 'outDv2' to the new Board cells 
            foreach ( Coordinate coordinate in newOccupiedCells2 )
            {
                Board[coordinate.X][coordinate.Y] = outDv2;
            }
            outDv2.Coordinate = outC2;
            // Colours the board in the two sequences of cells
            PolluteBoard(outDv1, sequenceOfCells1);
            PolluteBoard(outDv2, sequenceOfCells2);

            if (shouldPrint)
            {
                //
            }
        }

// ---------------------------------------------------------------------------
// DISPENSE METHODS

        // Takes a 'DropletInAction' and performs a dispense action.
        // Also queues and dequeues next pair of 'DropletInAction'.
        private void PerformDispense( DropletInAction dIA )
        {
            // TODO
            bool movesAreAvailable = BestDispenseCoordinate( dIA, out Dictionary<Coordinate, DropletVertex> moves );
            // If no moves are available:
            if (!movesAreAvailable)
            {
                throw new NoAvailableMoveException("No move available");
            }
            // Coordinates and the respective droplet vertices
            Coordinate outC1 = moves.Keys.ToList()[0];
            DropletVertex outDv1 = moves[outC1];
            Coordinate outC2 = moves.Keys.ToList()[1];
            DropletVertex outDv2 = moves[outC2];
            // Old and new occupied cells
            var oldOccupiedCells = gcf.NX( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume );
            var newOccupiedCells1 = gcf.NX( outC1, outDv1.Volume );
            var newOccupiedCells2 = gcf.NX( outC2, outDv2.Volume );
            // Queue and dequeue every 'DropletInAction'
            QueueNextDropletsInAction( dIA );
            DequeueDropletInAction( dIA.DropletVertex );
            // The sequence of cells that will be traversed by respectively outDv1 and outDv2
            List<Coordinate> sequenceOfCells1 = SequenceOfCells(dIA.DropletVertex.Coordinate, outC1);
            List<Coordinate> sequenceOfCells2 = SequenceOfCells(dIA.DropletVertex.Coordinate, outC2);
            // Remove the droplet that was split from the Board cells it currently occupies
            foreach ( Coordinate coordinate in oldOccupiedCells )
            {
                Board[coordinate.X][coordinate.Y] = null;
            }
            // Move 'outDv1' to the new Board cells
            foreach ( Coordinate coordinate in newOccupiedCells1 )
            {
                Board[coordinate.X][coordinate.Y] = outDv1;
            }
            outDv1.Coordinate = outC1;
            // Move 'outDv2' to the new Board cells 
            foreach ( Coordinate coordinate in newOccupiedCells2 )
            {
                Board[coordinate.X][coordinate.Y] = outDv2;
            }
            outDv2.Coordinate = outC2;
            // Colours the board in the two sequences of cells
            PolluteBoard(outDv1, sequenceOfCells1);
            PolluteBoard(outDv2, sequenceOfCells2);

            if (shouldPrint)
            {
                // TODO
            }
        }

// ---------------------------------------------------------------------------
// INITIALIZATION METHODS

        // Initializes the Board so that every grid cell initially
        // contains a reference to a droplet vertex root in an
        // operation graph if this cell is contained in the
        // droplet vertex interference region
        private void InitBoard()
        {
            this.Board = new List<List<DropletVertex?>>(this.gcf.Width);
            for (int i = 0; i < gcf.Width; i++)
            {
                this.Board.Add
                (
                    Enumerable.Range(0, gcf.Height)
                    .Select<int, DropletVertex?>(x => null).ToList()
                );
            }
            foreach (DropletVertex root in og.Roots)
            {
                foreach
                (
                    Coordinate cell in 
                    gcf.NX(root.Coordinate, root.Volume)
                )
                {
                    this.Board[cell.X][cell.Y] = root;
                }   
            }   
        }


        // Initializes 'DropletVertexToDropletInAction' dictionary.
        private void InitDropletVertexToDropletInAction()
        {
            dropletVertexToDropletInAction = new Dictionary<DropletVertex, DropletInAction>(og.DIdToOIdOut.Count);
            foreach ( int dropletId in og.DIdToOIdOut.Keys )
            {
                DropletVertex dv = og.DIdToDv[dropletId];
                OperationVertex ov =
                og.OIdToOv
                [
                    og.DIdToOIdOut[dropletId][0]
                ];
                Coordinate? targetCoordinate = null;
                DropletVertex? targetDropletVertex = null;
                int criticalDistance = 0;
                double mergeVolume = 0;
                int stepsLeftMaximum = ov.LittleT;
                int stepsLeftMinimum = ov.BigT;

                if ( ov.M is 1 )
                {
                    targetCoordinate = 
                    og.DIdToDv
                    [
                        og.OIdToDIdOut[ov.O][0]
                    ].Coordinate;
                }
                if ( ov.M is 2 )
                {
                    targetDropletVertex =
                    og.DIdToDv
                    [
                        og.OIdToDIdIn[ov.O].Single(x => x != dropletId)
                    ];
                    
                    criticalDistance = Metrics.ComputeCriticalDistance(dv.Volume, targetDropletVertex.Volume);
                    
                    mergeVolume =
                    og.DIdToDv[
                        og.OIdToDIdOut[ov.O][0]
                    ].Volume;
                }
                if ( ov.M is 3 )
                {
                    // NOTHING HAPPENS
                }
                if ( ov.M is 4 )
                {
                    // NOTHING HAPPENS 
                }
                if ( ov.M is 5 )
                {
                    // NOTHING HAPPENS
                }
                /*
                if ( ov.M is 6 )
                {
                    
                }
                */
                var dropletInAction = new DropletInAction
                (
                    dv,
                    ov,
                    targetCoordinate,
                    targetDropletVertex,
                    criticalDistance,
                    mergeVolume,
                    stepsLeftMaximum,
                    stepsLeftMinimum
                );
                dropletVertexToDropletInAction.Add( og.DIdToDv[dropletId], dropletInAction );
            }
        }


        // Initializes the dictionary 'executionQueue'
        private void InitExecutionQueue()
        {
            executionQueue = new Dictionary<DropletInAction, bool>(2*og.I_d.Count);
            foreach (DropletVertex dv in og.Roots)
            {
                executionQueue.Add( dropletVertexToDropletInAction[dv], true );
            }
        }

        // Given a 'DropletInAction' object 'dIA', queues the next 'DropletInAction' object
        private void QueueNextDropletsInAction( DropletInAction dIA )
        {
            // Select all 'DropletInAction' children that are not leaves
            List<DropletInAction> nextDropletsInAction =
            og.OIdToDIdOut[dIA.OperationVertex.O]
            .Where(
                nextDropletVertexId =>
                og.DIdToOIdOut.ContainsKey(nextDropletVertexId)
            ).Select(
                nextDropletVertexId =>
                dropletVertexToDropletInAction[og.DIdToDv[nextDropletVertexId]]
            ).ToList();
            // Queue all 'DropletInAction' children
            nextDropletsInAction.ForEach
            (
                nextDropletInAction =>
                executionQueue.Add
                (
                    nextDropletInAction,
                    true
                )
            );
        }

        // Given a 'DropletVertex' object, removes its corresponding 'DropletInAction'
        // from the queue. Used mainly for removing the partner of whatever droplet
        // we're passing to 'QueueNextDropletsInAction' in the method 'PerformMerge'.
        private void DequeueDropletInAction( DropletVertex dropletVertex )
        {
            executionQueue.Remove
            (
                dropletVertexToDropletInAction[ dropletVertex ]
            );
        }

// ---------------------------------------------------------------------------
// HELPING METHODS


        // Decides whether 'dropletVertex' can move to 'targetCoordinate'
        private bool CanMoveTo( DropletVertex dropletVertex, Coordinate targetCoordinate )
        {
            // Checks whether there is an occupying droplet vertex in the interference
            // zone border, and if so, decides whether 'dropletVertex' has the key to the
            // interference zone of the droplet vertex pointed to by the cell at 'targetCoordinate'.
            foreach ( Coordinate coordinate in gcf.NB(targetCoordinate, dropletVertex.Volume) )
            {
                // If the cell points to a non-null value:
                if ( Board[coordinate.X][coordinate.Y] is DropletVertex occupyingDropletVertex )
                {
                    // If the droplet with droplet id 'dropletId' can not
                    // enter the quarantine zone of the droplet with
                    // droplet id 'occupyingDropletId'
                    if ( og.K[dropletVertex.DropletId][occupyingDropletVertex.DropletId] is false)
                    {
                        return false;
                    }
                }
            }
            // Checks whether the occupation zone of 'targetCoordinate' is occupied by a droplet. If yes, then
            // checks whether 'dropletVertex' has the key to merge with occupying droplet. If instead no,
            // then checks whether 'dropletVertex' can  move on top of the pollution class
            // occupying the cell.
            foreach ( Coordinate coordinate in gcf.NX(targetCoordinate, dropletVertex.Volume) )
            {
                // If the cell points to a non-null value:
                if ( Board[coordinate.X][coordinate.Y] is DropletVertex occupyingDropletVertex )
                {
                    // If the droplet with droplet id 'dropletId' can not
                    // enter the quarantine zone of the droplet with
                    // droplet id 'occupyingDropletId'
                    if ( og.K[dropletVertex.DropletId][occupyingDropletVertex.DropletId] is false)
                    {
                        return false;
                    }
                }
                else if ( og.F[dropletVertex.Colour][CG[coordinate.X][coordinate.Y]] is false )
                    {
                        return false;
                    }
            }
            return true;
        }

        // If two droplets are going to touch and potentially merge we have
        // to make sure there is enough room for them to do so.
        // If merging droplets A and B results in droplet C, then C has a
        // limited list of cells its placement can be on.
        // Whether two droplets touch and merge depends on their volumes
        // and distances from each other. Thus, if two droplets are too close
        // they will have to move elsewhere.
        private bool TargetCellHasSpace( DropletInAction dIA, Coordinate target )
        {
            // If moving 'dIA.DropletVertex' to coordinate 'target' still maintains 
            // non-critical distance between 'dIA.DropletVertex' and 
            // 'dIA.TargetDropletVertex', then there's plenty room for the move
            if 
            (
                Metrics.HorizontalDistance(target, dIA.TargetDropletVertex.Coordinate) 
                >= dIA.CriticalDistance
                ||
                Metrics.VerticalDistance(target, dIA.TargetDropletVertex.Coordinate)
                >= dIA.CriticalDistance
            )
            {

                return true;
            }
            // Else we have to make sure that the resulting merged droplet
            // is going to occupy a cell with enough room
            else 
            {
                return gcf.CanSit(target, dIA.MergeVolume);
            }
        }

        // Given a droplet 'dv' with occupation radius 'radius', 
        // compute whether we can move it horizontally between coordinates
        //  'from' and 'to' without colliding with other droplets
        private bool CanMoveHorizontallyTo(DropletVertex dv, Coordinate from, Coordinate to, int radius)
        {  
            // If we don't move horizontally
            if (from.X == to.X)
            {
                return true;
            }
            // When we check validity we move along the x-axis from 'downY' to 'upY' 
            int leftX;
            int rightX;
            // If we want to move from left to right:
            if (to.X > from.X)
            {
                leftX = from.X;
                rightX = to.X;
            }
            // If we want to move from right to left:
            else
            {
                leftX = to.X;
                rightX = from.X;
            }
            // Checks that no foreign droplet is in the interference region of any
            // placement the droplet will have during its movement.
            // We want to move from leftmost interference column to rightmost 
            // interference column
            for (int i =  Math.Max(0, leftX - radius); i < Math.Min(rightX + radius + 1, gcf.Width); i++)
            {
                // And from lowest interference row to highest interference row
                for (int j = Math.Max(0, from.Y - radius); j < Math.Min(from.Y + radius + 1, gcf.Height); j++)
                {
                    // If a cell is occupied by a droplet that is different
                    // from the one we try to move
                    if ( Board[i][j] is DropletVertex occupyingDropletVertex
                        && occupyingDropletVertex != dv )
                    {
                        return false;
                    }
                }
            }
            // Checks whether the cells that 'dv' wants to occupy during its move
            // are eligible for traversal w.r.t the pollution classes occupying
            // said cells.  
            for (int i = leftX - radius + 1; i < rightX + radius; i++)
            {
                // And from lowest row of cells touched to highest row of cells touched
                for (int j = from.Y - radius + 1; j < from.Y + radius; j++)
                {
                    // If a cell is occupied by a droplet that is different
                    // from the one we try to move
                    if ( og.F[dv.Colour][CG[i][j]] is false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        // Given a droplet 'dv' with occupation radius 'radius', 
        // compute whether we can move it vertically between coordinates
        //  'from' and 'to' without colliding with other droplets
        private bool CanMoveVerticallyTo(DropletVertex dv, Coordinate from, Coordinate to, int radius)
        {  
            // If we don't move horizontally
            if (from.Y == to.Y)
            {
                return true;
            }
            // When we check validity we move along the x-axis from 'downY' to 'upY' 
            int downY;
            int upY;
            // If we want to move from left to right:
            if (to.Y > from.Y)
            {
                downY = from.Y;
                upY = to.Y;
            }
            // If we want to move from right to left:
            else
            {
                downY = to.X;
                upY = from.X;
            }
            // Checks that no foreign droplet is in the interference region of any
            // placement the droplet will have during its movement.
            // We want to move from lowest interference row to highest 
            // interference row
            for (int j =  Math.Max(0, downY - radius); j < Math.Min(upY + radius + 1, gcf.Height); j++)
            {
                // And from leftmost interference column to rightmost interference column
                for (int i = Math.Max(0, from.X - radius); i < Math.Min(from.X + radius + 1, gcf.Width); i++)
                {
                    // If a cell is occupied by a droplet that is different
                    // from the one we try to move
                    if ( Board[i][j] is DropletVertex occupyingDropletVertex
                        && occupyingDropletVertex != dv )
                    {
                        return false;
                    }
                }
            }
            // Checks whether the cells that 'dv' wants to occupy during its move
            // are eligible for traversal w.r.t the pollution classes occupying
            // said cells.  
            for (int j = downY - radius + 1; j < upY + radius; j++)
            {
                // And from leftmost column of cells touched to rightmost column of cells touched
                for (int i = from.X - radius + 1; i < from.X + radius; i++)
                {
                    // If a cell is occupied by a droplet that is different
                    // from the one we try to move
                    if ( og.F[dv.Colour][CG[i][j]] is false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }


        // Returns the best greedy coordinate to move the droplet to for a 'Move' operation
        private Coordinate BestMoveCoordinate( DropletInAction dIA )
        {
            List<Coordinate> neighbourhood =
            gcf.N( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume );
            
            Coordinate bestCoordinate = dIA.DropletVertex.Coordinate;
            int bestDistance = int.MaxValue;
            int currentDistance;

            foreach (var coordinate in neighbourhood)
            {
                currentDistance = Metrics.LP1( coordinate, dIA.TargetCoordinate );
                if
                (
                    currentDistance < bestDistance
                    && CanMoveTo( dIA.DropletVertex, coordinate )
                )
                {
                    bestDistance = currentDistance;
                    bestCoordinate = coordinate;
                }
            }
            return bestCoordinate;
        }

        // Returns the best greedy coordinate to move the droplet to for a 'Merge' operation
        private Coordinate BestMergeCoordinate( DropletInAction dIA )
        {
            List<Coordinate> neighbourhood =
            gcf.N( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume);

            Coordinate bestCoordinate = dIA.DropletVertex.Coordinate;
            int bestDistance = int.MaxValue;
            int currentDistance;
            Coordinate coordinateTarget = dIA.TargetDropletVertex.Coordinate;


            foreach (var coordinate in neighbourhood)
            {
                currentDistance = Metrics.LP1( coordinate, coordinateTarget );
                if
                (
                    currentDistance < bestDistance
                    && CanMoveTo( dIA.DropletVertex, coordinate )
                    && TargetCellHasSpace( dIA, coordinate )
                )
                {
                    // If the best coordinate to move to so far is the one already
                    // occupied by the droplet, then gives a chance to skip it.
                    // This helps exploration
                    if (coordinate.OnTopOf(dIA.DropletVertex.Coordinate))
                    {
                        if (random.NextDouble() < 0.25)
                        {
                            bestDistance = currentDistance;
                            bestCoordinate = coordinate;
                        }
                    }
                    // Otherwise always choose best coordinate to move to
                    else
                    {
                        bestDistance = currentDistance;
                        bestCoordinate = coordinate;
                    }
                }
            }
            // If it was not possible to move anywhere the droplet should remain
            // where it is
            if (bestDistance == int.MaxValue)
            {
                return dIA.DropletVertex.Coordinate;
            }
            return bestCoordinate;
        }

        // Returns the best coordinate to move the droplet to for a 'Mix' operation.
        // Coordinate is randomly picked. 
        // Can't move to 'previousPosition' since this would undo a previous move.
        // TODO: Later implement functionality so better moves are favored
        private Coordinate BestMixCoordinate( DropletInAction dIA )
        {
            // Filters all potential moves
           List<Coordinate> potentialMoves = 
            gcf.N( dIA.DropletVertex.Coordinate, dIA.DropletVertex.Volume )
            .Where
            (
                coordinate => 
                !coordinate.OnTopOf( dIA.PreviousPosition )
                && CanMoveTo( dIA.DropletVertex, coordinate )
            ).ToList();
            // Returns a random move from within potential moves
            Coordinate bestMove = potentialMoves
            [
                random.Next(potentialMoves.Count)
            ];
            // In case we move to a different position this counts
            // in the amount of mixing moves
            if (!dIA.DropletVertex.Coordinate.OnTopOf( bestMove ))
            {
                dIA.StepsLeftMinimum--;
                dIA.PreviousPosition = dIA.DropletVertex.Coordinate;
            }
            return bestMove;
        }

        // Picks a pair of coordinates that we can dispinse to with shortest
        // distance inbetween
        private bool BestDispenseCoordinate( DropletInAction dIA, out Dictionary<Coordinate, DropletVertex> moves )
        {   
            moves = new Dictionary<Coordinate, DropletVertex>(2);
            // The two 'DropletVertex' we split into
            DropletVertex outDv1 = 
            og.DIdToDv
            [
                og.OIdToDIdOut[dIA.OperationVertex.O][0]
            ];
            DropletVertex outDv2 = 
            og.DIdToDv
            [
                og.OIdToDIdOut[dIA.OperationVertex.O][1]
            ];
            // Holds the droplet we want to stay and the droplet we want to move
            DropletVertex stayDv;
            DropletVertex moveDv;
            // we move the droplet with the smallest volume
            if (outDv1.Volume > outDv2.Volume)
            {
                stayDv = outDv1;
                moveDv = outDv2;
            } else
            {
                stayDv = outDv2;
                moveDv = outDv1;
            }
            // Radii of 'outDv1' and 'outDv2' w.r.t cells occupied
            int stayRadius = gcf.ComputeRadius(stayDv.Volume);
            int moveRadius = gcf.ComputeRadius(moveDv.Volume);
            // 'outDv1' and 'outDv2' must be further apart than 'criticalDistance' both in
            // horizontal distance and in vertical distance
            int criticalDistance = Metrics.ComputeCriticalDistance(stayDv.Volume, moveDv.Volume);
            // Variables for keeping track of currently best result
            Coordinate bestCoordinate = null;
            int bestDistance = int.MaxValue;
            int currentDistance;
            // For every possible combination of ways that we can split the
            // the droplet so that the resulting droplets are further apart than
            // the critical distance, while still not colliding with other droplets
            // during the move 
            int stepRadius = criticalDistance;
            List<Coordinate> moveDvNeighbourhood = gcf.DispenseN (dIA.DropletVertex.Coordinate, moveRadius, stepRadius);
            foreach (Coordinate c in moveDvNeighbourhood)
            {
                if
                (
                    ( Metrics.HorizontalDistance(dIA.DropletVertex.Coordinate, c) >= criticalDistance
                    ||
                    Metrics.VerticalDistance(dIA.DropletVertex.Coordinate, c) >= criticalDistance )
                    &&
                    CanMoveHorizontallyTo(dIA.DropletVertex, dIA.DropletVertex.Coordinate, c, moveRadius)
                    &&
                    CanMoveVerticallyTo(dIA.DropletVertex, dIA.DropletVertex.Coordinate, c, moveRadius)
                )
                {
                    currentDistance = Metrics.LP1(dIA.DropletVertex.Coordinate, c);
                    if (Metrics.LP1(dIA.DropletVertex.Coordinate, c) < bestDistance)
                    {
                        bestCoordinate = c;
                        bestDistance = currentDistance;
                    }
                }
            }
            // If we found a suitable move
            if (bestDistance < int.MaxValue)
            {
                moves.Add(bestCoordinate, moveDv);
                moves.Add(dIA.DropletVertex.Coordinate, stayDv);
                return true;
            }
            // Else return false
            return false;
        }

        // Picks a pair of coordinates that we can split to with shortest
        // distance inbetween
        private bool BestSplitCoordinates( DropletInAction dIA, out Dictionary<Coordinate, DropletVertex> moves )
        {   
            moves = new Dictionary<Coordinate, DropletVertex>(2);
            // The two 'DropletVertex' we split into
            DropletVertex outDv1 = 
            og.DIdToDv
            [
                og.OIdToDIdOut[dIA.OperationVertex.O][0]
            ];
            DropletVertex outDv2 = 
            og.DIdToDv
            [
                og.OIdToDIdOut[dIA.OperationVertex.O][1]
            ];
            // Radii of 'outDv1' and 'outDv2' w.r.t cells occupied
            int dv1Radius = gcf.ComputeRadius(outDv1.Volume);
            int dv2Radius = gcf.ComputeRadius(outDv2.Volume);
            // 'outDv1' and 'outDv2' must be further apart than 'criticalDistance' both in
            // horizontal distance and in vertical distance
            int criticalDistance = Metrics.ComputeCriticalDistance(outDv1.Volume, outDv2.Volume);
            // Variables for keeping track of currently best result
            Coordinate bestC1 = null;
            Coordinate bestC2 = null;
            int bestDistance = int.MaxValue;
            int currentDistance;
            // For every possible combination of ways that we can split the
            // the droplet so that the resulting droplets are further apart than
            // the critical distance, while still not colliding with other droplets
            // during the move 
            for (int stepRadius1 = 1; stepRadius1 <= criticalDistance; stepRadius1++)
            {
                for (int stepRadius2 = 1; stepRadius2 <= criticalDistance; stepRadius2++)
                {
                    if (stepRadius1 + stepRadius2 < criticalDistance)
                    {
                        continue;
                    }
                    // Neighbourhood of possible cells 'outDv1' can occupy
                    List<Coordinate> dv1N = gcf.SplitN (dIA.DropletVertex.Coordinate, dv1Radius, stepRadius1);
                    // Neighbourhood of possible cells 'outDv2' can occupy
                    List<Coordinate> dv2N = gcf.SplitN (dIA.DropletVertex.Coordinate, dv2Radius, stepRadius2);
                    foreach (Coordinate c1 in dv1N)
                    {
                        foreach (Coordinate c2 in dv2N )
                        {
                            if
                            (
                                // If we find a single pair of coordinates far enough apart
                                // and without collision
                                ( Metrics.HorizontalDistance(c1, c2) >= criticalDistance
                                ||
                                Metrics.VerticalDistance(c1, c2) >= criticalDistance )
                                &&
                                CanMoveHorizontallyTo(dIA.DropletVertex, dIA.DropletVertex.Coordinate, c1, dv1Radius)
                                &&
                                CanMoveVerticallyTo(dIA.DropletVertex, dIA.DropletVertex.Coordinate, c1, dv1Radius)
                                &&
                                CanMoveHorizontallyTo(dIA.DropletVertex, dIA.DropletVertex.Coordinate, c2, dv2Radius)
                                &&
                                CanMoveVerticallyTo(dIA.DropletVertex, dIA.DropletVertex.Coordinate, c2, dv2Radius)
                            )
                            {   
                                currentDistance = Metrics.LP1(c1, c2);
                                if (Metrics.LP1(c1, c2) < bestDistance)
                                {
                                    bestC1 = c1;
                                    bestC2 = c2;
                                    bestDistance = currentDistance;
                                }
                            }
                        }
                    }
                }
            }
            // If we found a suitable move
            if (bestDistance < int.MaxValue)
            {
                moves.Add(bestC1, outDv1);
                moves.Add(bestC2, outDv2);
                return true;
            }
            // Else return false
            return false;
        }

        // Given two Coordinates 'from' and 'to' that lie either in 
        // a horizontal line or a vertical line, return all Coordinates
        // that line in this line
        private IEnumerable<Coordinate?> GetCoordinatesInMoveLine (Coordinate from, Coordinate to)
        {
            int xLeft;
            int xRight;
            int yDown;
            int yUp;
            // at least one of the two conditions hold by our logic so far:
            // xLeft == xRight or yDown == yUp.
            // Potentially both
            xLeft = from.X < to.X ? from.X : to.X;
            xRight = from.X < to.X ? to.X : from.X;
            yDown = from.Y < to.Y ? from.Y : to.Y;
            yUp = from.Y < to.Y ? to.Y : from.Y;
            // If the move is horizontal
            if (yDown == yUp)
            {
                return 
                (
                    from xCoordinate in Enumerable.Range(xLeft, xRight)
                    select new Coordinate(yDown, xCoordinate)
                );
            }
            // If the move is vertical
            else
            {
                return
                (
                    from yCoordinate in Enumerable.Range(yDown, yUp)
                    select new Coordinate(xLeft, yCoordinate)
                );
            }
        }

        // Returns an ordered list of cells in one direction that a droplet vertex
        // will traverse.
        // Only use this method when either either 'from.X == to.X' or 'from.Y == to.Y' holds
        private List<Coordinate> SequenceOfCells(Coordinate start, Coordinate end)
        {
            // Translation from integer to direction:
            // 0 is left, 1 is right, 2 is down, 3 is up
            int direction;
            int count;
            // If move is left or right
            if (start.Y == end.Y)
            {
                direction = start.X > end.X ? 0 : 1;
                count = start.X > end.X ? start.X - end.X + 1 : end.X - start.X + 1;
            }
            // If move is up or down
            else
            {
                direction = start.Y > end.Y ? 2 : 3;
                count = start.Y > end.Y ? start.Y - end.Y + 1 : end.Y - start.Y + 1;
            }
            // Holds the moves
            var sequenceOfCells = new List<Coordinate>(count);
            switch (direction)
            {   
                // If move is left
                case 0:
                    sequenceOfCells = 
                    (
                        from x in Enumerable.Range(end.X, count).Reverse()
                        select new Coordinate(x, start.Y)
                    ).ToList();
                    break;
                // If move is right
                case 1:
                    sequenceOfCells = 
                    (
                        from x in Enumerable.Range(start.X, count)
                        select new Coordinate(x, start.Y)
                    ).ToList();
                    break;
                // If move is down
                case 2:
                    sequenceOfCells = 
                    (
                        from y in Enumerable.Range(end.Y, count).Reverse()
                        select new Coordinate(start.X, y)
                    ).ToList();
                    break;
                // If move is up
                case 3:
                    sequenceOfCells = 
                    (
                        from y in Enumerable.Range(start.Y, count)
                        select new Coordinate(start.X, y)
                    ).ToList();
                    break;
            }
            return sequenceOfCells;
        }

        // Given a droplet vertex and a sequence of cells it will occupy,
        // pollute all cells the given droplet vertex will touch in its traversal
        private void PolluteBoard(DropletVertex dv, List<Coordinate> cellSequence)
        {
            List<Coordinate> occupiedCells;
            foreach (Coordinate sequenceCoordinate in cellSequence)
            {
                occupiedCells = gcf.NX( sequenceCoordinate, dv.Volume );
                // The colour of the droplet at cell 'sequenceCoordinate'
                // should become a mix of all the colours of the cells it will
                // occupy after the move
                foreach ( Coordinate occupiedCoordinate in occupiedCells )
                {
                    dv.Colour = og.E
                    [ dv.Colour ]
                    [ CG [ occupiedCoordinate.X ] [ occupiedCoordinate.Y ] ];
                }
                // Colour all the cells in 'occupiedCells'
                foreach ( Coordinate occupiedCoordinate in occupiedCells )
                {
                    CG[occupiedCoordinate.X][occupiedCoordinate.Y]
                    = dv.Colour;
                }
            }
        }

// ---------------------------------------------------------------------------
// PRINTING METHODS

        // Returns the string output for printing a given 'DropletInAction'
        private string PrintDropletInAction( DropletInAction dIA )
        {
            // If 'dIA' is a DropletInAction moving towards a coordinate:
            if (dIA.TargetCoordinate is Coordinate coordinate)
            {
                return
                String.Format
                (
                    "Dv:  {0}, ({1}, {2}), {3}, {4}\n"
                    + "Ov:  {5}, {6}, {7}, {8}, {9}\n"
                    + "TargetCoordinate:  ({10}, {11})\n",
                    dIA.DropletVertex.DropletId,
                    dIA.DropletVertex.Coordinate.X,
                    dIA.DropletVertex.Coordinate.Y,
                    dIA.DropletVertex.Volume,
                    dIA.DropletVertex.Colour,
                    dIA.OperationVertex.O,
                    dIA.OperationVertex.M,
                    dIA.OperationVertex.BigT,
                    dIA.OperationVertex.LittleT,
                    dIA.OperationVertex.P,
                    dIA.TargetCoordinate.X,
                    dIA.TargetCoordinate.Y
                );
            }
            // If 'dIA' is a DropletInAction moving towards another DropletInAction:
            else if (dIA.TargetDropletVertex is DropletVertex targetDropletVertex)
            {
                return
                String.Format
                (
                    "Dv:  {0}, ({1}, {2}), {3}, {4}\n"
                    + "Ov:  {5}, {6}, {7}, {8}, {9}\n"
                    + "TargetDv:  {10}, ({11}, {12}), {13}, {14}\n",
                    dIA.DropletVertex.DropletId,
                    dIA.DropletVertex.Coordinate.X,
                    dIA.DropletVertex.Coordinate.Y,
                    dIA.DropletVertex.Volume,
                    dIA.DropletVertex.Colour,
                    dIA.OperationVertex.O,
                    dIA.OperationVertex.M,
                    dIA.OperationVertex.BigT,
                    dIA.OperationVertex.LittleT,
                    dIA.OperationVertex.P,
                    targetDropletVertex.DropletId,
                    targetDropletVertex.Coordinate.X,
                    targetDropletVertex.Coordinate.Y,
                    targetDropletVertex.Volume,
                    targetDropletVertex.Colour
                );
            }
            // If 'dIA' has no specified target to move towards
            else
            {
                return
                String.Format
                (
                    "Dv:  {0}, ({1}, {2}), {3}, {4}\n"
                    + "Ov:  {5}, {6}, {7}, {8}, {9}\n"
                    + "TargetDv:  *EMPTY* \n",
                    dIA.DropletVertex.DropletId,
                    dIA.DropletVertex.Coordinate.X,
                    dIA.DropletVertex.Coordinate.Y,
                    dIA.DropletVertex.Volume,
                    dIA.DropletVertex.Colour,
                    dIA.OperationVertex.O,
                    dIA.OperationVertex.M,
                    dIA.OperationVertex.BigT,
                    dIA.OperationVertex.LittleT,
                    dIA.OperationVertex.P
                );
            }
        }

        // Prints valuable information for each call to 'PerformMerge'
        private void PrintMerge
        ( DropletInAction dIA, Coordinate target, 
            DropletVertex resultingDropletVertex )
        {
            mergeCount ++;
            string dIAToPrint = 
            PrintDropletInAction(dIA);

            string coordinateTargetToPrint =
            String.Format
            (
                "coordinateTarget:  ({0}, {1})\n",
                target.X,
                target.Y
            );

            string resultingDropletVertexToPrint =
            String.Format
            (
                "Dv:  {0}, ({1}, {2}), {3}, {4}",
                resultingDropletVertex.DropletId,
                resultingDropletVertex.Coordinate.X,
                resultingDropletVertex.Coordinate.Y,
                resultingDropletVertex.Volume,
                resultingDropletVertex.Colour
            );

            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("Merge count:   {0}", mergeCount);
            Console.WriteLine(dIAToPrint);
            Console.WriteLine(coordinateTargetToPrint);
            Console.WriteLine(resultingDropletVertexToPrint);
        }

        // Prints the content of 'executionQueue'
        private void PrintExecutionQueue(int iterationNumber)
        {
            Console.WriteLine("\n \n");
            Console.WriteLine("-----------------");
            Console.WriteLine("executionQueue in iteration:  {0}\n", iterationNumber);
            foreach (DropletInAction dIA in executionQueue.Keys)
            {
                Console.WriteLine(PrintDropletInAction(dIA));
            }
        }

    }
}