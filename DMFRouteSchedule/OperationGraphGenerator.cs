using System;
using System.IO;

namespace DMFRouteSchedule
{
    // When we want to generate an operation graph
    class OperationGraphGenerator
    {
        // Our randomizer
        private Random rand;
        // Holds all coordinates a droplet with radius respectively 1, 2 or 3
        // can occupy. Used when picking a random for a newly constructed droplet
        private List<Coordinate> possibleCoordinatesToOccupy1;
        private List<Coordinate> possibleCoordinatesToOccupy2;
        private List<Coordinate> possibleCoordinatesToOccupy3;
        // Holds all droplet vertices currently present to perform operations on
        private List<DropletVertex> currentDropletVertices;
        // Holds all strings describing droplet vertices 
        private List<string> dropletVertexStrings;
        // Holds all strings describing operation vertices
        private List<string> operationVertexStrings;
        // Holds all strings describing droplet vertices and their operation vertex out-neighbours
        private List<string> dropletIdsToOperationIdStrings;
        // Holds all strings describing operation vertices and their droplet vertex out-neighbours
        private List<string> operationIdToDropletIdsStrings;

        // Constructor for the generator environment
        public OperationGraphGenerator(int seed)
        {
            rand = new Random(seed);
        }

        // Generates an operation graph from .txt-file with name 'inputFileName'
        // and gives the output name 'outputFileName' by performing
        // 'steps' amount of steps, where 'pNew' gives the chance of constructing
        // a new droplet instead of using previous droplets and 'pOperation' is
        // a list assigning chances to add an operation with the number 'index+1'; 
        // that is, pOpereation is a discrete distribution.
        //
        // Uses the ruleset that only the 'Merge' operation changes the droplet's
        // colour (look at the comments by Neeti in the LaTeX-file)
        public void GenerateGraph
        (
            string inputFileName, string outputFileName, int steps, double pNew, 
            double[] pOperation, DropletInfo dInfo, OperationInfo oInfo, int seed
        )
        {
            rand = new Random(seed);
            // Initialize 
            string textInput = File.ReadAllText(inputFileName);
            string[] textInputLines = textInput.Split("\n", StringSplitOptions.RemoveEmptyEntries);
            // Holds all droplet vertices that currently exist in the board 
            currentDropletVertices =
            (from dropletString in textInputLines select InitDropletVertex(dropletString)).ToList();
            // Initialize some lists, yo
            dropletVertexStrings = currentDropletVertices.Select(dv => writeDropletVertex(dv)).ToList();
            operationVertexStrings = new List<string>();
            dropletIdsToOperationIdStrings = new List<string>();
            operationIdToDropletIdsStrings = new List<string>();
            // Assuming that 'dropletVertices' is ordered by droplet ids from low to high,
            // 'nextDId' is the droplet id the next droplet constructed should have
            int nextDId = currentDropletVertices[currentDropletVertices.Count-1].DropletId + 1;
            // 'nextOId' is the operation id the next operation constructed should have
            int nextOId = 0;
            // Assuming that there exist no gaps between the colour classes of
            // the droplets, 'nextColour' is the colour the next droplet constructed should have
            int nextColour = currentDropletVertices.Select( x => x.Colour ).Max() + 1;
            int step = 0;
            int operationNumber;
            while ( step < steps )
            {
                operationNumber = Sample(pOperation) + 1;
                // If 'operationNumber' denotes a merge but there are not enough droplets
                // to merge, we instead do something else
                if (operationNumber == 2 && currentDropletVertices.Count < 2)
                {
                    continue;
                }
                addOperation( operationNumber, dInfo, oInfo, ref nextDId, ref nextOId, ref nextColour );
                step++;
            }
            var output = dropletVertexStrings
                .Append("|")
                .Concat(operationVertexStrings)
                .Append("|")
                .Concat(dropletIdsToOperationIdStrings)
                .Append("|")
                .Concat(operationIdToDropletIdsStrings);
                                            
            File.WriteAllLines(outputFileName, output);
        }

        // Taking an operation graph in .txt-format, generates a list of rules
        // for colour mixing and writes this in .txt-format.
        // Assumes the only way to change colours of droplets is to merge them.
        // Thus, the only rules we have to consider with care are
        //  1.  0 and colour A should always give colour A,
        //  2.  colour A and colour A should always give colour A,
        //  3.  colour A and B merging should always give colour C
        // Any other colour combination will never be tried, hence we can default
        // and say such combinations should result in colour 0
        public void GenerateRules
        (
            string inputFileName, string outputFileName
        )
        {
            // Splits the input up into the four different parts of the operation graph input
            string[] parts = File.ReadAllText(inputFileName).Split("|");
            // The dictionary containing all colours at some time present in the board
            var colours = new Dictionary<string, bool>(); 
            parts[0].Split("\n", StringSplitOptions.RemoveEmptyEntries)
            .ToList()
            .ForEach
            ( 
                line => 
                colours.TryAdd( line.Split(", ")[3], true)
            );
            // Variable used to hold '.Split(...)' results in the following 'foreach' loops
            string[] split;
            // Translation from droplet id to the colour of it
            var dIdToColour = new Dictionary<string, string>();
            foreach ( string dvString in parts[0].Split("\n", StringSplitOptions.RemoveEmptyEntries) )
            {
                split = dvString.Split(", ");
                dIdToColour.TryAdd
                (
                    split[0],
                    split[3]
                );
            }
            // droplet ids to operation id relation
            var dIdsToOId = new Dictionary<string, string>();
            foreach ( string dIdsToOIdString in parts[2].Split("\n", StringSplitOptions.RemoveEmptyEntries) )
            {
                split = dIdsToOIdString.Split(" -> ");
                dIdsToOId.TryAdd
                (
                    split[0],
                    split[1]
                );
            }
            // operation id to droplet ids relation
            var oIdToDIds = new Dictionary<string, string>();
            foreach ( string oIdToDIdsString in parts[3].Split("\n", StringSplitOptions.RemoveEmptyEntries) )
            {
                split = oIdToDIdsString.Split(" -> ");
                oIdToDIds.TryAdd
                (
                    split[0],
                    split[1]
                );
            }

            // Anonymous function for splitting keys
            Func<string, string[]> splitDIds = dIds => dIds.Split("; ");
            // Construct the relations of the form 'C1, C2, C3'
            var relations = new List<string>();
            // For each relation describing a merge
            foreach
            (
                string inDIds in 
                (
                    from dIds in dIdsToOId.Keys
                    let dIdsSplit = splitDIds(dIds)
                    where dIdsSplit[0] != "_" && dIdsSplit[1] != "_"
                    select dIds
                )
            )
            // Associate the input droplet ids with the output droplet id,
            // translate into colours and add to list of strings to print
            {
                string inColourFirst = dIdToColour.GetValueOrDefault( inDIds.Split("; ")[0] );
                string inColourSecond = dIdToColour.GetValueOrDefault( inDIds.Split("; ")[1] );
                // if the merge actually merges two droplets of the same colour, 
                // then we don't want to add a new rule
                if ( inColourFirst == inColourSecond )
                {
                    continue;
                } 

                // Otherwise...
                string outDId = 
                splitDIds
                (
                    oIdToDIds.GetValueOrDefault
                    (
                        dIdsToOId.GetValueOrDefault(inDIds)
                    )
                )[0];

                relations.Add
                (
                    String.Format
                    (
                        "{0}, {1}, {2}",
                        inColourFirst,
                        inColourSecond,
                        dIdToColour.GetValueOrDefault( outDId )
                    )
                );

                relations.Add
                (
                    String.Format
                    (
                        "{0}, {1}, {1}",
                        dIdToColour.GetValueOrDefault( inDIds.Split("; ")[0] ),
                        dIdToColour.GetValueOrDefault( outDId )
                    )
                );

                relations.Add
                (
                    String.Format
                    (
                        "{0}, {1}, {1}",
                        dIdToColour.GetValueOrDefault( inDIds.Split("; ")[1] ),
                        dIdToColour.GetValueOrDefault( outDId )
                    )
                );
            }                      
            // Adds the first part of the mixing-rules .txt-format.
            // That is, the list of colour ids
            string output = "0\n";
            colours.Keys
                .ToList()
                .ForEach
                (
                    key =>
                    output = output + key + "\n"
                );
            output = output + "|\n";
            // adds the second part of the mixing-rules .txt-format.
            // That is, the list specifying the rules
            relations
                .ForEach
                (
                    relation =>
                    output = output + relation + "\n"
                );
            
            File.WriteAllText(outputFileName, output);
        }
        
        // Generates all operation graphs with seeds from 0 to 'numberOfOperationGraphs'
        // with 'numberOfSteps' amount of operations in them, given an input graph generator
        // 'graphInput'. Puts the generated graphs into 'graphOutputDirectory'
        // and the generated rules into 'rulesOutputDirectory'
        public void GenerateSeveral
        (
            string graphInputDirectory, string graphInputName,
            string graphOutputDirectory, string rulesOutputDirectory,
            int numberOfSteps, int numberOfOperationGraphs,
            double pNew, double[] pOperation, DropletInfo dInfo, OperationInfo oInfo

        )
        {
            string graphOutputName;
            string rulesOutputName;
            for (int i = 0; i < numberOfOperationGraphs; i++)
            {
                // Generate graph
                graphOutputName = graphOutputDirectory 
                + graphInputName.Substring(0, graphInputName.Count() - 4)
                + "-generated-graph" 
                + String.Format("{0}.txt", i+1);
                Console.WriteLine(graphOutputName);
                GenerateGraph
                (
                    graphInputDirectory + graphInputName, graphOutputName, numberOfSteps, 
                    pNew, pOperation, dInfo, oInfo, seed: i
                );
                // Generate rules for the recently generated graph
                rulesOutputName = rulesOutputDirectory
                + graphInputName.Substring(0, graphInputName.Count() - 4)
                + "-generated-rules" 
                + String.Format("{0}.txt", i+1);
                Console.WriteLine(rulesOutputName);
                GenerateRules
                (
                    inputFileName: graphOutputName, outputFileName: rulesOutputName
                );
            }
        }


// ---------------------------------------------------------------------------
// METHODS NECESSARY FOR 'GenerateGraph(...)'

        // Writes the corresponding characters in the 'allParts' tuple
        // given we want to add 'action' for which we construct new droplet vertices
        // if 'makeNew' is true, and otherwise use already existing droplet vertices
        private void addOperation
        (
            int operationNumber,
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {
            switch (operationNumber)
            {
                case 1:
                    addMove(dInfo, oInfo, ref dId, ref oId, ref colour);
                    break;
                case 2:
                    addMerge(dInfo, oInfo, ref dId, ref oId, ref colour);
                    break;
                case 3:
                    addMix(dInfo, oInfo, ref dId, ref oId, ref colour);
                    break;
                case 4:
                    addSplit(dInfo, oInfo, ref dId, ref oId, ref colour);
                    break;
                case 5:
                    addDispense(dInfo, oInfo, ref dId, ref oId, ref colour);
                    break;
                case 6:
                    addHeat(dInfo, oInfo, ref dId, ref oId, ref colour);
                    break;
            }
        }

        private void addMove
        (
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {
            // Randomly pick a droplet vertex to move
            int index = rand.Next(0, currentDropletVertices.Count);
            DropletVertex dvBeforeMove = currentDropletVertices[index];
            // Remove 'dvBeforeMove' from list of current droplet vertices
            // since we should no longer be able to pick it
            currentDropletVertices.RemoveAt(index);
            // Construct the droplet following from the move, taking a random
            // feasible placement
            int dvRadius = ComputeRadius(dvBeforeMove.Volume);
            var newCoordinate = new Coordinate
            (
                rand.Next(0 + dvRadius - 1, 20 - dvRadius + 1), 
                rand.Next(0 + dvRadius - 1, 32 - dvRadius + 1)
            );
            DropletVertex dvAfterMove = 
            new DropletVertex
            (
                dropletId: dId,
                coordinate: newCoordinate,
                volume: dvBeforeMove.Volume,
                colour: dvBeforeMove.Colour
            );
            // Increment 'dId'
            dId++;
            // Write the string for the new droplet vertex
            dropletVertexStrings.Add(writeDropletVertex(dvAfterMove));
            // Write the string for the new operation vertex
            operationVertexStrings.Add
            (
                writeOperationVertex
                (
                    operationId: oId,
                    moveName: "Move",
                    T: rand.Next(oInfo.BigTIntervalLowerBound, oInfo.BigTIntervalUpperBound),
                    t: null,
                    p: 1
                )
            );
            // Write the droplet ids to operation id relation string
            dropletIdsToOperationIdStrings.Add
            (
                writeDropletsToOperationRelation
                (
                    dropletIdFirst: dvBeforeMove.DropletId,
                    dropletIdSecond: null,
                    operationId: oId
                )
            );
            // Write the operation ids to droplet id relation string
            operationIdToDropletIdsStrings.Add
            (
                writeOperationToDropletsRelation
                (
                    operationId: oId,
                    dropletIdFirst: dvAfterMove.DropletId,
                    dropletIdSecond: null
                )
            );
            // Increment 'oId'
            oId++;
            // Update current droplet vertices by adding 'dvAfterMerge'
            currentDropletVertices.Add(dvAfterMove);
        }

        private void addMerge
        (
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {
            // Randomly pick two droplets to merge
            int indexFirst = rand.Next(0, currentDropletVertices.Count);
            DropletVertex dvBeforeMergeFirst = currentDropletVertices[indexFirst];
            // Remove 'dvBeforeMergeFirst' from list of current droplet vertices
            // since we should no longer be able to pick it
            currentDropletVertices.RemoveAt(indexFirst);
            int indexSecond = rand.Next(0, currentDropletVertices.Count);
            DropletVertex dvBeforeMergeSecond = currentDropletVertices[indexSecond];
            // Remove 'dvBeforeMergeSecond' from list of current droplet vertices
            // since we should no longer be able to pick it
            currentDropletVertices.RemoveAt(indexSecond);
            // Construct the droplet following from the merge.
            DropletVertex dvAfterMerge;
            // If the two droplets we merge have equal colour classes,
            // then the resulting droplet will also have equal colour class.
            // Otherwise we assign the colour passed.
            if (dvBeforeMergeFirst.Colour == dvBeforeMergeSecond.Colour)
            {
                dvAfterMerge = 
                new DropletVertex
                (
                    dropletId: dId,
                    coordinate: new Coordinate(null, null),
                    volume: dvBeforeMergeFirst.Volume + dvBeforeMergeSecond.Volume,
                    colour: dvBeforeMergeFirst.Colour
                );
            } else
            {
                dvAfterMerge = 
                new DropletVertex
                (
                    dropletId: dId,
                    coordinate: new Coordinate(null, null),
                    volume: dvBeforeMergeFirst.Volume + dvBeforeMergeSecond.Volume,
                    colour: colour
                );
                // Increment colour'
                colour++;
            }
            // Increment 'dId'
            dId++;
            // Write the string for the new droplet vertex
            dropletVertexStrings.Add(writeDropletVertex(dvAfterMerge));
            // Write the string for the new operation vertex
            operationVertexStrings.Add
            (
                writeOperationVertex
                (
                    operationId: oId,
                    moveName: "Merge",
                    T: rand.Next(oInfo.BigTIntervalLowerBound, oInfo.BigTIntervalUpperBound),
                    t: null,
                    p: 1
                )
            );
            // Write the droplet ids to operation id relation string
            dropletIdsToOperationIdStrings.Add
            (
                writeDropletsToOperationRelation
                (
                    dropletIdFirst: dvBeforeMergeFirst.DropletId,
                    dropletIdSecond: dvBeforeMergeSecond.DropletId,
                    operationId: oId
                )
            );
            // Write the operation id to droplet ids relation string
            operationIdToDropletIdsStrings.Add
            (
                writeOperationToDropletsRelation
                (
                    operationId: oId,
                    dropletIdFirst: dvAfterMerge.DropletId,
                    dropletIdSecond: null
                )
            );
            // Increment 'oId'
            oId++;
            // Update current droplet vertices by removing 'dvBeforeMove'
            // and adding 'dvAfterMerge'
            currentDropletVertices.Add(dvAfterMerge);
        }

        private void addMix
        (
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {
            // Randomly pick a droplet vertex to move
            int index = rand.Next(0, currentDropletVertices.Count);
            DropletVertex dvBeforeMix = currentDropletVertices[index];
            // Remove 'dvBeforeMove' from list of current droplet vertices
            // since we should no longer be able to pick it
            currentDropletVertices.RemoveAt(index);
            // Construct the droplet resulting from the mix
            DropletVertex dvAfterMix = 
            new DropletVertex
            (
                dropletId: dId,
                coordinate: new Coordinate(null, null),
                volume: dvBeforeMix.Volume,
                colour: dvBeforeMix.Colour
            );
            // Increment 'dId'
            dId++;
            // Write the string for the new droplet vertex
            dropletVertexStrings.Add(writeDropletVertex(dvAfterMix));
            // Write the string for the new operation vertex
            operationVertexStrings.Add
            (
                writeOperationVertex
                (
                    operationId: oId,
                    moveName: "Mix",
                    T: rand.Next(oInfo.BigTIntervalLowerBound, oInfo.BigTIntervalUpperBound),
                    t: rand.Next(oInfo.LittleTIntervalLowerBound, oInfo.LittleTIntervalUpperBound),
                    p: 1
                )
            );
            // Write the droplet ids to operation id relation string
            dropletIdsToOperationIdStrings.Add
            (
                writeDropletsToOperationRelation
                (
                    dropletIdFirst: dvBeforeMix.DropletId,
                    dropletIdSecond: null,
                    operationId: oId
                )
            );
            // Write the operation ids to droplet id relation string
            operationIdToDropletIdsStrings.Add
            (
                writeOperationToDropletsRelation
                (
                    operationId: oId,
                    dropletIdFirst: dvAfterMix.DropletId,
                    dropletIdSecond: null
                )
            );
            // Increment 'oId'
            oId++;
            // Update current droplet vertices by adding 'dvAfterMix'
            currentDropletVertices.Add(dvAfterMix);
        }

        private void addSplit
        (
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {
            // Randomly pick a droplet vertex to move
            int index = rand.Next(0, currentDropletVertices.Count);
            DropletVertex dvBeforeSplit = currentDropletVertices[index];
            // Remove 'dvBeforeMove' from list of current droplet vertices
            // since we should no longer be able to pick it
            currentDropletVertices.RemoveAt(index);
            // Construct the droplets resulting from the split,
            // sampling a random split volume
            double splitPercentage = RandomDouble(0.5, 1);
            DropletVertex dvAfterSplitFirst =
            new DropletVertex
            (
                dropletId: dId,
                coordinate: new Coordinate(null, null),
                volume: splitPercentage*dvBeforeSplit.Volume,
                colour: dvBeforeSplit.Colour

            );
            // increment 'dId'
            dId++;
            DropletVertex dvAfterSplitSecond = 
            new DropletVertex
            (
                dropletId: dId,
                coordinate: new Coordinate(null, null),
                volume: (1 - splitPercentage)*dvBeforeSplit.Volume,
                colour: dvBeforeSplit.Colour
            );
            // Increment 'dId'
            dId++;
            // Write the strings for the new droplet vertices
            dropletVertexStrings.Add(writeDropletVertex(dvAfterSplitFirst));
            dropletVertexStrings.Add(writeDropletVertex(dvAfterSplitSecond));
            // Write the string for the new operation vertex
            operationVertexStrings.Add
            (
                writeOperationVertex
                (
                    operationId: oId,
                    moveName: "Split",
                    T: null,
                    t: null,
                    p: splitPercentage
                )
            );
            // Write the droplet ids to operation id relation string
            dropletIdsToOperationIdStrings.Add
            (
                writeDropletsToOperationRelation
                (
                    dropletIdFirst: dvBeforeSplit.DropletId,
                    dropletIdSecond: null,
                    operationId: oId
                )
            );
            // Write the operation ids to droplet id relation string
            operationIdToDropletIdsStrings.Add
            (
                writeOperationToDropletsRelation
                (
                    operationId: oId,
                    dropletIdFirst: dvAfterSplitFirst.DropletId,
                    dropletIdSecond: dvAfterSplitSecond.DropletId
                )
            );
            // Increment 'oId'
            oId++;
            // Update current droplet vertices by adding 'dvAfterMix'
            currentDropletVertices.Add(dvAfterSplitFirst);
            currentDropletVertices.Add(dvAfterSplitSecond);
        }

        private void addDispense
        (
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {
            // Randomly pick a droplet vertex to move
            int index = rand.Next(0, currentDropletVertices.Count);
            DropletVertex dvBeforeDispense = currentDropletVertices[index];
            // Remove 'dvBeforeMove' from list of current droplet vertices
            // since we should no longer be able to pick it
            currentDropletVertices.RemoveAt(index);
            // Construct the droplets resulting from the split,
            // sampling a random split volume
            double dispensePercentage = RandomDouble(0.5, 1);
            DropletVertex dvAfterDispenseFirst =
            new DropletVertex
            (
                dropletId: dId,
                coordinate: new Coordinate(null, null),
                volume: dispensePercentage*dvBeforeDispense.Volume,
                colour: dvBeforeDispense.Colour

            );
            // increment 'dId'
            dId++;
            DropletVertex dvAfterDispenseSecond = 
            new DropletVertex
            (
                dropletId: dId,
                coordinate: new Coordinate(null, null),
                volume: (1 - dispensePercentage)*dvBeforeDispense.Volume,
                colour: dvBeforeDispense.Colour
            );
            // Increment 'dId'
            dId++;
            // Write the strings for the new droplet vertices
            dropletVertexStrings.Add(writeDropletVertex(dvAfterDispenseFirst));
            dropletVertexStrings.Add(writeDropletVertex(dvAfterDispenseSecond));
            // Write the string for the new operation vertex
            operationVertexStrings.Add
            (
                writeOperationVertex
                (
                    operationId: oId,
                    moveName: "Dispense",
                    T: null,
                    t: null,
                    p: dispensePercentage
                )
            );
            // Write the droplet ids to operation id relation string
            dropletIdsToOperationIdStrings.Add
            (
                writeDropletsToOperationRelation
                (
                    dropletIdFirst: dvBeforeDispense.DropletId,
                    dropletIdSecond: null,
                    operationId: oId
                )
            );
            // Write the operation ids to droplet id relation string
            operationIdToDropletIdsStrings.Add
            (
                writeOperationToDropletsRelation
                (
                    operationId: oId,
                    dropletIdFirst: dvAfterDispenseFirst.DropletId,
                    dropletIdSecond: dvAfterDispenseSecond.DropletId
                )
            );
            // Increment 'oId'
            oId++;
            // Update current droplet vertices by adding 'dvAfterMix'
            currentDropletVertices.Add(dvAfterDispenseFirst);
            currentDropletVertices.Add(dvAfterDispenseSecond);
        }

        private void addHeat
        (
            DropletInfo dInfo,
            OperationInfo oInfo,
            ref int dId,
            ref int oId,
            ref int colour
        )
        {

        }

// ---------------------------------------------------------------------------
// WRITNG METHODS AND INITIALIZATION METHODS

        // Initializes a droplet vertex from the given input
        // "d, u, x, c"
        private DropletVertex InitDropletVertex(string dropletString)
        {
            string[] split = dropletString.Split(", ");
            if (split.Count() != 4)
            {
                var errorMessage = String.Format
                (
                    "Droplet vertex does not contain exactly three commas: {0}",
                    dropletString
                );
                throw new InputNotFeasibleException(errorMessage);
            }
            return new DropletVertex(split[0], split[1], split[2], split[3]);
        }


        // Writes a droplet vertex into the correct format
        private string writeDropletVertex(DropletVertex dv)
        {
            if (dv.Coordinate.Uninstantiated)
            {
                return
                String.Format
                (
                    "{0}, _, {1}, {2}",
                    dv.DropletId,
                    dv.Volume,
                    dv.Colour
                );
            }
            else
            {
                return
                String.Format
                (
                    "{0}, {1};{2}, {3}, {4}",
                    dv.DropletId,
                    dv.Coordinate.X,
                    dv.Coordinate.Y,
                    dv.Volume,
                    dv.Colour
                );
            }
        }

        // Takes operationVertex specifications and writes it into correct format,
        // giving a 20% chance to ignore 'T' or 't' instead uninstantiate these values
        private string writeOperationVertex(int operationId, string moveName, int? T, int? t, double p)
        {
            return
            String.Format
            (
                "{0}, {1}, {2}, {3}, {4}",
                operationId,
                moveName,
                T is int ? (rand.NextDouble() > 0.2 ? T : "_") : "_",
                t is int ? (rand.NextDouble() > 0.2 ? t : "_") : "_",
                p is double ? p : "_"
            );
        }

        // Takes droplet ids to operation id relationship and writes it into correct format
        private string writeDropletsToOperationRelation(int dropletIdFirst, int? dropletIdSecond, int operationId)
        {
            return
            String.Format
            (
                "{0}; {1} -> {2}",
                dropletIdFirst,
                dropletIdSecond is int ? dropletIdSecond : "_",
                operationId
            );
        }

        // Takes operation id to droplet ids relationship and writes it into correct format
        private string writeOperationToDropletsRelation(int operationId, int dropletIdFirst, int? dropletIdSecond)
        {
            return
            String.Format
            (
                "{0} -> {1}; {2}",
                operationId,
                dropletIdFirst,
                dropletIdSecond is int ? dropletIdSecond : "_"
            );
        }

// ---------------------------------------------------------------------------
// HELPING METHODS

        // Samples an index of the given discrete distribution where the value
        // at each index denotes the probability for sampling the value
        private int Sample(double[] dist)
        {
            double draw = rand.NextDouble();
            int i = 0;
            double interval = 0.0;
            while (i < dist.Count())
            {
                interval += dist[i];
                if (draw < interval)
                {
                    break;
                }
                i ++;
            }
            return i;
        }

        // Samples a random double within the interval (lowerBound, upperBound)
        private double RandomDouble(double lowerBound, double upperBound)
        {
            return lowerBound + ( (upperBound - lowerBound) * rand.NextDouble() );
        }

        // Translates the volume of a droplet into a radius of 
        // the board cells it occupies.
        private int ComputeRadius(double volume)
        {
            if ( volume < 2.3 )
            {
                return 1;
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return 2;
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return 3;
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return 3;
        }
    }

    // Contains information about how to construct droplets randomly.
    // Specifies intervals which the different DropletVertex attributes should lie within
    public class DropletInfo
    {
        public int VolumeIntervalLowerBound { get; }
        public int VolumeIntervalUpperBound { get; }

        public DropletInfo ( int volumeIntervalLowerBound, int volumeIntervalUpperBound )
        {
            VolumeIntervalLowerBound = volumeIntervalLowerBound;
            VolumeIntervalUpperBound = volumeIntervalUpperBound;
        }
    }

    // Contains information about how to construct operations randomly
    // Specifies intervals which the different OperationVertex attributes should lie within
    public class OperationInfo
    {
        public int BigTIntervalLowerBound { get; set; }
        public int BigTIntervalUpperBound { get; set; }
        public int LittleTIntervalLowerBound { get; set; }
        public int LittleTIntervalUpperBound { get; set; }
        public double PIntervalLowerBound { get; set; }
        public double PIntervalUpperBound { get; set; }
        

        public OperationInfo
        (
            int bigTIntervalLowerBound, int bigTIntervalUpperBound,
            int littleTIntervalLowerBound, int littleTIntervalUpperBound,
            double pIntervalLowerBound, double pIntervalUpperBound
        )
        {
            BigTIntervalLowerBound = bigTIntervalLowerBound;
            BigTIntervalUpperBound = bigTIntervalUpperBound;
            LittleTIntervalLowerBound = littleTIntervalLowerBound;
            LittleTIntervalUpperBound = littleTIntervalUpperBound;
            PIntervalLowerBound = pIntervalLowerBound;
            PIntervalUpperBound = pIntervalUpperBound;
        }
    }

}