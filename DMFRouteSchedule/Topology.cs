using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMFRouteSchedule
{   
    class GridConfiguration
    {
        
        public int Width { get; set; }
        public int Height { get; set; }
        // Neighbourhoods
        public List<List<List<Coordinate>>> N1 { get; set; }
        public List<List<List<Coordinate>>> N2 { get; set; }
        public List<List<List<Coordinate>>> N3 { get; set; }
        // Interference regions. So far handles up to a radius of 3.
        // We need these to account for varying droplet sizes. 
        // Can add more later if needed.
        public List<List<List<Coordinate>>> NI1 { get; set; }
        public List<List<List<Coordinate>>> NI2 { get; set; }
        public List<List<List<Coordinate>>> NI3 { get; set; }
        // Occupation regions. The radius of NXi is one smaller than the radius
        // of NIi, where i is an integer.
        public List<List<List<Coordinate>>> NX1 { get; set; }
        public List<List<List<Coordinate>>> NX2 { get; set; }
        public List<List<List<Coordinate>>> NX3 { get; set; }
        // The border cells of every interference region.
        // For some integer i we define NBi = NIi - NXi
        public List<List<List<Coordinate>>> NB1 { get; set; }
        public List<List<List<Coordinate>>> NB2 { get; set; }
        public List<List<List<Coordinate>>> NB3 { get; set; }
        // Denotes whether a droplet of radius respectively 1, 2 or 3
        // can sit at a given position in the board
        public List<List<bool>> CanSit1 { get; set; }
        public List<List<bool>> CanSit2 { get; set; }
        public List<List<bool>> CanSit3 { get; set; }
        // Special vertices in the grid
        private List<(Coordinate Coordinate, int type)> VS;

        public GridConfiguration(int width, int height, List<(Coordinate Coordinate, int type)> specialVertices)
        {
            Width = width;
            Height = height;
            N1 = NeighbourhoodFunctions.NH1(width, height, 1);
            N2 = NeighbourhoodFunctions.NH1(width, height, 2);
            N3 = NeighbourhoodFunctions.NH1(width, height, 3);
            NI1 = InterferenceRegionFunctions.IR1(width, height, 1);
            NI2 = InterferenceRegionFunctions.IR1(width, height, 2);
            NI3 = InterferenceRegionFunctions.IR1(width, height, 3);
            VS = specialVertices;
            InitNX();
            InitNB();
            InitCanSit();
        }

        // Translates the volume of a droplet into a radius of 
        // the board cells it occupies.
        public int ComputeRadius(double volume)
        {
            if ( volume < 2.3 )
            {
                return 1;
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return 2;
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return 3;
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return 3;
        }

        // Returns the correct interference region.
        //
        // (1)
        // We assume that a droplet has center point in the center
        // of whichever cell it occupies.
        //
        // (2)
        // We assume that every droplet has a perfectly circular contour
        // where it touches the surface.
        //
        // (3)
        // We assume that a droplet of volume W residing on the surface
        // gets pressured down and gets the exact form of one half
        // of a 2W volume sphere obtained by cutting such sphere 
        // directly in half.
        //
        // Each cell is a square with 2mm side lengths. By (1) and (2), 
        //
        //  *1 for a droplet to occupy more than a single cell its radius
        //  must be greater than 1 mm,
        //
        //  *2 for a droplet to occupy more than 9 cells its radius
        //  must be greater than 3 mm,
        //
        //  *3 for a droplet to occupy more than 25 cell its radius
        //  must be greater than 5 mm,
        //
        // and so on. 
        //
        // The volume formula of a sphere with radius r is
        // V = 4/3*pi*r^3.
        //
        // By (3) this volume formula becomes V = 2/3*pi*r^3
        // for droplets lying on a surface.
        //
        // By plotting the required radii from *1, *2 and *3 into the
        // above formula we find that 
        //
        //  a droplet with less than 2/3*pi volume occupies a single cell,
        //  a droplet with volume between 2/3*pi and 18*pi occupies 9 cells,
        //  a droplet with volume between 18*pi and 250/3*pi occupies 25 cells.

        public List<Coordinate> NI(Coordinate c, double volume)
        {
            if ( volume < 2.3 )
            {
                return NI1[c.X][c.Y];
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return NI2[c.X][c.Y];
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return NI3[c.X][c.Y];
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return NI3[c.X][c.Y];
            
        }

        // Returns the correct occupation region.
        public List<Coordinate> NX(Coordinate c, double volume)
        {
            if ( volume < 2.3 )
            {
                return NX1[c.X][c.Y];
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return NX2[c.X][c.Y];
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return NX3[c.X][c.Y];
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return NX3[c.X][c.Y];
        }

        // Returns the correct neighbourhood function.
        public List<Coordinate> N(Coordinate c, double volume)
        {
            if ( volume < 2.3 )
            {
                return N1[c.X][c.Y];
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return N2[c.X][c.Y];
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return N3[c.X][c.Y];
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return N3[c.X][c.Y];
        }

        // Like 'N(Coordinate c, double volume)', but assumes 'width' and 'height' of board
        // have already been instantiated, and then computes a list of possible moves
        // given 'c', 'volume' and 'cellsPerStep'.
        // 'radius' is the radius of cells occupied.
        // 'cellsPerStep' is the amount of cells a droplet can traverse in one direction
        // per step
        public List<Coordinate> DispenseN(Coordinate c, int radius, int cellsPerStep = 1)
        {
            var neighbourhood = new List<Coordinate>(5);
            // Compute possible cells to move to
            if (c.X - radius - cellsPerStep + 1 >= 0)
            {
                neighbourhood.Add(new Coordinate(c.X-cellsPerStep, c.Y));
            }
            if (c.X + radius + cellsPerStep - 1 < Width)
            {
                neighbourhood.Add(new Coordinate(c.X+cellsPerStep, c.Y));
            }
            if (c.Y - radius - cellsPerStep + 1 >= 0)
            {
                neighbourhood.Add(new Coordinate(c.X, c.Y-cellsPerStep));
            }
            if (c.Y + radius + cellsPerStep - 1 < Height)
            {
                neighbourhood.Add(new Coordinate(c.X, c.Y+cellsPerStep));
            }
            neighbourhood.Add(c);

            return neighbourhood;
        }

        // Like 'DispenseN' but does not include the coordinate itself in the
        // neighbourhood (so when used in combination with a move method we only
        // allow moves to different coordinates)
        public List<Coordinate> SplitN(Coordinate c, int radius, int cellsPerStep = 1)
        {
            var neighbourhood = new List<Coordinate>(5);
            // Compute possible cells to move to
            if (c.X - radius - cellsPerStep + 1 >= 0)
            {
                neighbourhood.Add(new Coordinate(c.X-cellsPerStep, c.Y));
            }
            if (c.X + radius + cellsPerStep - 1 < Width)
            {
                neighbourhood.Add(new Coordinate(c.X+cellsPerStep, c.Y));
            }
            if (c.Y - radius - cellsPerStep + 1 >= 0)
            {
                neighbourhood.Add(new Coordinate(c.X, c.Y-cellsPerStep));
            }
            if (c.Y + radius + cellsPerStep - 1 < Height)
            {
                neighbourhood.Add(new Coordinate(c.X, c.Y+cellsPerStep));
            }
            return neighbourhood;
        }

        // Returns whether a droplet of volume 'volume' can sit at
        // coordinate 'c'
        public bool CanSit(Coordinate c, double volume)
        {
            if ( volume < 2.3 )
            {
                return CanSit1[c.X][c.Y];
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return CanSit2[c.X][c.Y];
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return CanSit3[c.X][c.Y];
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return CanSit3[c.X][c.Y];
        }

        // Returns the correct border region region.
        public List<Coordinate> NB(Coordinate c, double volume)
        {
            if ( volume < 2.3 )
            {
                return NB1[c.X][c.Y];
            }
            if ( volume >= 2.3 && volume < 18 )
            {
                return NB2[c.X][c.Y];
            }
            if ( volume >= 18 && volume < 83.33 )
            {
                return NB3[c.X][c.Y];
            }
            // Included to fool the compiler. We should never return
            // this far into the method
            return NB3[c.X][c.Y];
        }

        // ---------------------------------------------------------------------------
        // INITIALIZATION METHODS

        // Initializes the occupation regions
        private void InitNX()
        {
            NX1 = new List<List<List<Coordinate>>>(Width);
            NX2 = new List<List<List<Coordinate>>>(Width);
            NX3 = new List<List<List<Coordinate>>>(Width);
            for (int i = 0; i < Width; i++)
            {
                NX1.Add( new List<List<Coordinate>>(Height) );
                NX2.Add( new List<List<Coordinate>>(Height) );
                NX3.Add( new List<List<Coordinate>>(Height) );
                for (int j = 0; j < Height; j++)
                {
                    NX1[i].Add( new List<Coordinate>(1) { new Coordinate(i, j) } );
                    NX2[i].Add( NI1[i][j] );
                    NX3[i].Add( NI2[i][j] );
                }
            }
        }

        private void InitNB()
        {
            NB1 = new List<List<List<Coordinate>>>(Width);
            NB2 = new List<List<List<Coordinate>>>(Width);
            NB3 = new List<List<List<Coordinate>>>(Width);
            for (int i = 0; i < Width; i++)
            {
                NB1.Add( new List<List<Coordinate>>(Height) );
                NB2.Add( new List<List<Coordinate>>(Height) );
                NB3.Add( new List<List<Coordinate>>(Height) );
                for (int j = 0; j < Height; j++)
                {
                    NB1[i].Add( NI1[i][j].Where(u => !NX1[i][j].Contains(u)).ToList() );
                    NB2[i].Add( NI2[i][j].Where(u => !NX2[i][j].Contains(u)).ToList() );
                    NB3[i].Add( NI3[i][j].Where(u => !NX3[i][j].Contains(u)).ToList() );
                }
            }
        }

        // Inits the 'CanSit' lists of lists
        private void InitCanSit()
        {
            CanSit1 = new List<List<bool>>(Width);
            CanSit2 = new List<List<bool>>(Width);
            CanSit3 = new List<List<bool>>(Width);
            for (int i = 0; i < Width; i++)
            {
                CanSit1.Add( new List<bool>(Height) );
                CanSit2.Add( new List<bool>(Height) );
                CanSit3.Add( new List<bool>(Height) );
                for (int j = 0; j < Height; j++)
                {                  
                    CanSit1[i].Add(true);

                    CanSit2[i].Add
                    ( 
                        (
                            i - 1 >= 0 
                            && i + 1 < Width
                            && j - 1 >= 0 
                            && j + 1 < Height
                        ) ? true : false 
                    );

                    CanSit3[i].Add
                    (
                        (
                            i - 2 >= 0 
                            && i + 2 < Width
                            && j - 2 >= 0 
                            && j + 2 < Height
                        ) ? true : false
                    );
                }
            }
        }
    }

    // Contains all functions that can be used to initialize the neighbourhood list
    static class NeighbourhoodFunctions
    {
        // First attempt at a neighbourhood function.
        // Radius is the amount of cells occupied by a droplet from its center
        // to its border. I.e., 
        //  a radius of 1 means 1 cell,
        //  a radius of 2 means 9 cells,
        //  a radius of 3 means 25 cells and so on.
        public static List<List<List<Coordinate>>> NH1( int width, int height, int radius )
        {
            var neighbourhood = new List<List<List<Coordinate>>>(width);
            for (int i = 0; i < width; i ++)
            {
                neighbourhood.Add(new List<List<Coordinate>>(height));
                for (int j = 0; j < height; j ++)
                {
                    neighbourhood[i].Add(new List<Coordinate>(4));
                    if (i - radius >= 0)
                    {
                        neighbourhood[i][j].Add(new Coordinate(i-1, j));
                    }
                    if (i + radius < width)
                    {
                        neighbourhood[i][j].Add(new Coordinate(i+1, j));
                    }
                    if (j - radius >= 0)
                    {
                        neighbourhood[i][j].Add(new Coordinate(i, j-1));
                    }
                    if (j + radius < height)
                    {
                        neighbourhood[i][j].Add(new Coordinate(i, j+1));
                    }
                    neighbourhood[i][j].Add(new Coordinate(i, j));
                }
            }
            return neighbourhood;
        }
    }

    // Contains all functions that can be used to initialize the interference region list
    static class InterferenceRegionFunctions
    {
        // First attempt at an interference region function
        public static List<List<List<Coordinate>>> IR1(int width, int height, int radius)
        {
            var interferenceRegion = new List<List<List<Coordinate>>>(width);
            for (int i = 0; i < width; i ++)
            {
                interferenceRegion.Add(new List<List<Coordinate>>(height));
                for (int j = 0; j < height; j ++)
                {
                    interferenceRegion[i].Add(new List<Coordinate>(9));
                    int i_left = Math.Max(0, i-radius);
                    int i_right = Math.Min(i+radius, width - 1);
                    int j_low = Math.Max(0, j-radius);
                    int j_high = Math.Min(j+radius, height - 1);

                    for (int a = i_left; a < i_right + 1; a ++)
                    {
                        for (int b = j_low; b < j_high + 1; b ++)
                        {
                            interferenceRegion[i][j].Add(new Coordinate(a, b));
                        }
                    }      
                }
            }
            return interferenceRegion;
        }
    }
}