using System;
using System.Diagnostics;

namespace DMFRouteSchedule
{
    class ExecutorTest
    {
        private OperationGraph og;
        private GridConfiguration gcf;
        // A collection of CG's throughout time. Initially set to contain 500, but can vary.
        public List<List<List<int>>> CGCollection { get; set; }
        // A collection of boards throughout time. Initially set to contain 500, but can vary.
        public List<List<List<DropletVertex?>>> BoardCollection { get; set; }

        public ExecutorTest( OperationGraph og, GridConfiguration gcf, int seed )
        {
            this.gcf = gcf;
            this.og = og;
            InitCGCollection(500);
            InitBoardCollection(500);
        }

        // Initializes 'CGCollection'
        private void InitCGCollection(int initialSizeOfCollection)
        {
            CGCollection = new List<List<List<int>>>(initialSizeOfCollection);
            for (int time = 0; time < initialSizeOfCollection; time++)
            {
                var CG = new List<List<int>>(gcf.Width);
                for (int i = 0; i < gcf.Width; i++)
                {
                    CG.Add
                    (
                        Enumerable.Range(0, gcf.Height)
                        .Select( x => 0 ).ToList()
                    );
                }
                CGCollection.Add(CG);
            }
        }

        // Initializes 'BoardCollection'
        private void InitBoardCollection(int initialSizeOfCollection)
        {
            BoardCollection = new List<List<List<DropletVertex?>>>(initialSizeOfCollection);
            for (int time = 0; time < initialSizeOfCollection; time++)
            {
                var board = new List<List<DropletVertex?>>(gcf.Width);
                for (int i = 0; i < gcf.Width; i++)
                {
                    board.Add
                    (
                        Enumerable.Range(0, gcf.Height)
                        .Select<int, DropletVertex?>( x => null ).ToList()
                    );
                }
                BoardCollection.Add(board);
            }
        }

// ---------------------------------------------------------------------------
// TESTING MODIFICATION IN CGCollection

        // Assuming we use naive dijkstra to compute a path with O(2*|V|^2) and a constant factor
        // of 100 as overload per iteration, this gives us an overload of 20*32*20*32*100 <= 4100000.
        public void TestCGCollectionTime(int amountOfModificationsPerStep, int amountOfSteps, int extraWorkPerModification )
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < amountOfSteps; i++)
            {
                for (int j = 0; j < amountOfModificationsPerStep; j++)
                {
                    SetCGSequence(timeStart: 0, timeEnd: 100, x: 10, y: 10);
                    ResetCGSequence(timeStart: 0, timeEnd: 100, x: 10, y: 10);
                    
                    int counter = 0;
                    while (counter < extraWorkPerModification)
                    {
                        if (counter % 20000 == 0)
                        {
                            // Nothing
                        }
                        counter++;
                    }
                }
            }

            stopwatch.Stop();
            Console.WriteLine("Elapsed Time is {0} ms", stopwatch.ElapsedMilliseconds);
        }

        private void SetCGSequence(int timeStart, int timeEnd, int x, int y)
        {
            for (int i = timeStart; i < timeEnd + 1; i ++)
            {
                SetSquare(i, x, y);
            }
        }

        private void ResetCGSequence(int timeStart, int timeEnd, int x, int y)
        {
            for (int i = timeStart; i < timeEnd + 1; i ++)
            {
                ResetSquare(i, x, y);
            }
        }

        private void SetSquare(int time, int x, int y)
        {
            for (int i = x - 2; i < x + 3; i++)
            {
                for (int j = y - 2; j < y + 3; j++)
                {
                    CGCollection[time][i][j] = 1;
                }
            }

        }

        private void ResetSquare(int time, int x, int y)
        {
            for (int i = x - 2; i < x + 3; i++)
            {
                for (int j = y - 2; j < y + 3; j++)
                {
                    CGCollection[time][i][j] = 0;
                }
            }
        }

    }



}