using System;
using System.Diagnostics;

namespace DMFRouteSchedule
{
    class Printer
    {
        private int height;
        private int width;
        private GridConfiguration gcf;
        private OperationGraph og;
        private Executor executor;

        public Printer(GridConfiguration gcf, OperationGraph og, Executor executor)
        {
            height = gcf.Height;
            width = gcf.Width;
            this.gcf = gcf;
            this.og = og;
            this.executor = executor;
        }


//---------------------------------------------------------------
// functions for printing to the terminal

        // Prints the board populated with droplet ids.
        // IMPORTANT: only works with boards containing droplet ids not 
        // exceeding two digits.
        public void PrintBoardDropletIds()
        {
            String line = "-";
            for (int i = 0; i < width; i++)
            {
                line = line + "----";
                //line = String.Format(line + "{0}", "----");
            }

            Console.WriteLine(line);
            for (int j = 0; j < height; j++)
            {
                String contentLine = "|";
                for (int i = 0; i < width; i++)
                {
                    DropletVertex? cellContent = executor.Board[i][j];
                    if ( cellContent is null )
                    {
                        contentLine = String.Format
                        (
                            contentLine + "   |"
                        );
                    }
                    else
                    {
                        string cellContentString = executor.Board[i][j].DropletId.ToString();
                        contentLine = String.Format
                        ( 
                            contentLine + "{0}",
                            cellContentString.Length == 1 ? 
                            " " + cellContentString + " |" 
                            : 
                            " " + cellContentString + "|"
                        );
                    } 
                }
                Console.WriteLine(contentLine);
                Console.WriteLine(line);
            }     
        }

        // Prints the board populated with colour ids.
        // IMPORTANT: only works with boards containing colour ids not 
        // exceeding two digits.
        public void PrintCG()
        {
            String line = "-";
            for (int i = 0; i < width; i++)
            {
                line = line + "----";
                //line = String.Format(line + "{0}", "----");
            }

            Console.WriteLine(line);
            for (int j = 0; j < height; j++)
            {
                String contentLine = "|";
                for (int i = 0; i < width; i++)
                {
                    int cellContent = executor.CG[i][j];
                    string cellContentString = executor.CG[i][j].ToString();
                    if ( cellContent is 0 )
                    {
                        contentLine = String.Format
                        (
                            contentLine + "   |"
                        );
                    }
                    else
                    {
                        contentLine = String.Format
                        ( 
                            contentLine + "{0}",
                            cellContentString.Length == 1 ? 
                            " " + cellContentString + " |" 
                            : 
                            " " + cellContentString + "|"
                        );
                    }
                }
                Console.WriteLine(contentLine);
                Console.WriteLine(line);
            }
        }

//---------------------------------------------------------------
// functions for writing to .txt-files


        // Prints the board populated with droplet ids.
        // IMPORTANT: only works with boards containing droplet ids not 
        // exceeding two digits.
        public void WriteBoardDropletIds(string fileName, List<List<DropletVertex?>> board)
        {
            string filePath = "execution-output/droplet-id-output/text-format/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            String line = "-";
            for (int i = 0; i < width; i++)
            {
                line = line + "----";
            }

            sw.WriteLine(line);
            for (int j = 0; j < height; j++)
            {
                String contentLine = "|";
                for (int i = 0; i < width; i++)
                {
                    DropletVertex? cellContent = board[i][j];
                    if ( cellContent is null )
                    {
                        contentLine = contentLine + "   |";
                    }
                    else
                    {
                        string cellContentString = board[i][j].DropletId.ToString();
                        contentLine = String.Format
                        ( 
                            contentLine + "{0}",
                            cellContentString.Length == 1 ? 
                            " " + cellContentString + " |" 
                            : 
                            " " + cellContentString + "|"
                        );
                    } 
                }
                sw.WriteLine(contentLine);
                sw.WriteLine(line);
            } 
        }

        // Write the same as method 'WriteBoardDropletIds', but instead in a
        // format that can interpretted by the python script 'visualizer.py'.
        // 'outputDirectory' should point to the directory of the output for the given
        // graph. For example, 'execution-output/graph-generator1-generated-graph3/'
        public void WriteBoardDropletIdsPythonFormat(string outputDirectory, string fileName, List<List<DropletVertex?>> board)
        {
            if ( !Directory.Exists(outputDirectory) )
            {
                InitializeDirectory(outputDirectory);
            }
            string filePath = outputDirectory + "droplet-id-output/python-format/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            string dropletIds = "";
            foreach (int dropletId in og.I_d)
            {
                dropletIds = dropletIds + dropletId.ToString() + " ";
            }
            sw.WriteLine(dropletIds);

            for (int j = 0; j < height; j++)
            {
                String contentLine = "";
                for (int i = 0; i < width; i++)
                {
                    DropletVertex? cellContent = board[i][j];
                    if ( cellContent is null )
                    {
                        contentLine = contentLine + "x ";
                    }
                    else
                    {
                        string cellContentString = board[i][j].DropletId.ToString();
                        contentLine = contentLine + cellContentString + " ";
                    } 
                }
                sw.WriteLine(contentLine);
            } 
        }

        // Prints the board populated with colour ids.
        // IMPORTANT: only works with boards containing colour ids not 
        // exceeding two digits.
        public void WriteCG(string fileName, List<List<int>> cg)
        {
            string filePath = "execution-output/droplet-colour-output/text-format/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            String line = "-";
            for (int i = 0; i < width; i++)
            {
                line = line + "----";
            }

            sw.WriteLine(line);
            for (int j = 0; j < height; j++)
            {
                String contentLine = "|";
                for (int i = 0; i < width; i++)
                {
                    int cellContent = cg[i][j];
                    string cellContentString = cg[i][j].ToString();
                    if ( cellContent is 0 )
                    {
                        contentLine = contentLine + "   |";
                    }
                    else
                    {
                        contentLine = String.Format
                        ( 
                            contentLine + "{0}",
                            cellContentString.Length == 1 ? 
                            " " + cellContentString + " |" 
                            : 
                            " " + cellContentString + "|"
                        );
                    }
                }
                sw.WriteLine(contentLine);
                sw.WriteLine(line);
            }
        }

        // Write the same as method 'WriteCG', but instead in a
        // format that can interpretted by the python script 'visualizer.py'
        public void WriteCGPythonFormat(string outputDirectory, string fileName, List<List<int>> cg)
        {
            if ( !Directory.Exists(outputDirectory) )
            {
                InitializeDirectory(outputDirectory);
            }
            string filePath = outputDirectory + "droplet-colour-output/python-format/" + fileName + ".txt";
            StreamWriter sw = File.CreateText(filePath);
            sw.AutoFlush = true;

            string colourClasses = "";
            for ( int colourClass = 0; colourClass < og.E.Count; colourClass++ )
            {
                colourClasses = colourClasses + colourClass.ToString() + " ";
            }
            sw.WriteLine(colourClasses);

            for (int j = 0; j < height; j++)
            {
                String contentLine = "";
                for (int i = 0; i < width; i++)
                {
                    int cellContent = cg[i][j];
                    string cellContentString = cg[i][j].ToString();
                    contentLine = contentLine + cellContentString + " ";
                }
                sw.WriteLine(contentLine); 
            }
        }

        // Writes CG and droplet id outputs to pictures using the
        // 'visualize.py' python script
        public void WritePicturesPython(string outputDirectory)
        {
            if ( !Directory.Exists(outputDirectory) )
            {
                InitializeDirectory(outputDirectory);
            }
            var process = new Process();
            process.StartInfo.FileName = "python3";
            process.StartInfo.Arguments = String.Format("visualize.py {0}", outputDirectory);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
        }
//---------------------------------------------------------------
// functions for resetting the file path environments and creating directories

        public void Reset()
        {
            Directory.Delete("execution-output", true);
            Directory.CreateDirectory("execution-output");
        }

        public void InitializeDirectory(string directory)
        {
            Directory.CreateDirectory(directory);

            Directory.CreateDirectory(directory + "droplet-id-output/");
            Directory.CreateDirectory(directory + "droplet-id-output/text-format");
            Directory.CreateDirectory(directory + "droplet-id-output/pictures");
            Directory.CreateDirectory(directory + "droplet-id-output/python-format");

            Directory.CreateDirectory(directory + "droplet-colour-output");
            Directory.CreateDirectory(directory + "droplet-colour-output/text-format");
            Directory.CreateDirectory(directory + "droplet-colour-output/pictures");
            Directory.CreateDirectory(directory + "droplet-colour-output/python-format");

            Directory.CreateDirectory(directory + "droplet-colour-and-id-output");
            Directory.CreateDirectory(directory + "droplet-colour-and-id-output/text-format");
            Directory.CreateDirectory(directory + "droplet-colour-and-id-output/pictures");
            Directory.CreateDirectory(directory + "droplet-colour-and-id-output/python-format");
        }
    }
}