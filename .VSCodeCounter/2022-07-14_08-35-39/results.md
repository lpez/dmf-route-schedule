# Summary

Date : 2022-07-14 08:35:39

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  4238 codes, 941 comments, 337 blanks, all 5516 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 11 | 3,924 | 827 | 257 | 5,008 |
| Python | 1 | 303 | 114 | 80 | 497 |
| XML | 1 | 11 | 0 | 0 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 13 | 4,238 | 941 | 337 | 5,516 |
| DMFRouteSchedule | 13 | 4,238 | 941 | 337 | 5,516 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)