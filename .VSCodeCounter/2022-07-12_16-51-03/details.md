# Details

Date : 2022-07-12 16:51:03

Directory /home/kjoh/dmf-route-schedule

Total : 13 files,  3915 codes, 883 comments, 312 blanks, all 5110 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DMFRouteSchedule/Coordinate.cs](/DMFRouteSchedule/Coordinate.cs) | C# | 33 | 4 | 3 | 40 |
| [DMFRouteSchedule/DMFExceptions.cs](/DMFRouteSchedule/DMFExceptions.cs) | C# | 31 | 0 | 3 | 34 |
| [DMFRouteSchedule/DMFRouteSchedule.csproj](/DMFRouteSchedule/DMFRouteSchedule.csproj) | XML | 11 | 0 | 0 | 11 |
| [DMFRouteSchedule/Execution.cs](/DMFRouteSchedule/Execution.cs) | C# | 1,012 | 310 | 65 | 1,387 |
| [DMFRouteSchedule/Main.cs](/DMFRouteSchedule/Main.cs) | C# | 19 | 54 | 12 | 85 |
| [DMFRouteSchedule/Metrics.cs](/DMFRouteSchedule/Metrics.cs) | C# | 60 | 12 | 9 | 81 |
| [DMFRouteSchedule/OperationGraph.cs](/DMFRouteSchedule/OperationGraph.cs) | C# | 270 | 37 | 19 | 326 |
| [DMFRouteSchedule/OperationGraphGenerator.cs](/DMFRouteSchedule/OperationGraphGenerator.cs) | C# | 488 | 106 | 37 | 631 |
| [DMFRouteSchedule/OperationGraphInitializer.cs](/DMFRouteSchedule/OperationGraphInitializer.cs) | C# | 1,228 | 155 | 53 | 1,436 |
| [DMFRouteSchedule/Printer.cs](/DMFRouteSchedule/Printer.cs) | C# | 250 | 26 | 24 | 300 |
| [DMFRouteSchedule/Schedule.cs](/DMFRouteSchedule/Schedule.cs) | C# | 16 | 17 | 6 | 39 |
| [DMFRouteSchedule/Topology.cs](/DMFRouteSchedule/Topology.cs) | C# | 313 | 95 | 23 | 431 |
| [DMFRouteSchedule/visualize.py](/DMFRouteSchedule/visualize.py) | Python | 184 | 67 | 58 | 309 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)